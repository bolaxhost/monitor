﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonServices; 

namespace Model
{
    /// <summary>
    /// Конечный автомат алгоритма поиска (Горизонтальный поиск в две итерации)
    /// </summary>
    public class Searching
    {
        public struct SearchingResults
        {
            public double azimuth;
            public double placeAngle;
        }

        public event EventHandler<SearchingResults> OnSuccess;

        private Model.AnglesModel _angles = null;
        
        private uint _wait = 0;
        private uint _cwait = 0;
        private uint _stage = 0;

        private double? _targetAzimuth = null;
        private double? _targetPlaceAngle = null;

        private double[][] _path = null;
        private int _pathIndex = 0;

        private double _prevSignal = 0;
        private double _bestSignal = 0;

        private double? _bestAzimuth = null;
        private double? _bestPlaceAngle = null;

        private readonly double _StageOneRange;
        private readonly double _StageOneStep;
        private readonly double _StageTwoRange;
        private readonly double _StageTwoStep;


        /// <summary>
        /// Инициализация поиска
        /// </summary>
        /// <param name="angles">Текущая ориентация антенны</param>
        public Searching(Model.AnglesModel angles
            , double stageOneRange = 10
            , double stageOneStep = 1
            , double stageTwoRange  = 2
            , double stageTwoStep = 0.2
            )
        {
            _angles = angles;
            _cwait = _wait = 1;

            _StageOneRange = stageOneRange;
            _StageOneStep = stageOneStep;
            _StageTwoRange = stageTwoRange;
            _StageTwoStep = stageTwoStep;

            Console.WriteLine($"ПОИСК: Инициализация с параметрами I:{_StageOneRange} {_StageOneStep}; II:{_StageTwoRange} {_StageTwoStep}"); 
        }

        /// <summary>
        /// Обработка шага алгоритма
        /// </summary>
        /// <param name="signal">Текущий уровень сигнала</param>
        /// <param name="acm">Ссылка на блок управления антенной</param>
        public void Do(double signal, IACMControl acm, Model.AnglesModel currentAngles = null)
        {
            if (_completed)
                return;

            //Обработка текущего состояния антенны (контроль завернения поворота)
            if (_wait > 3 && _cwait < _wait && currentAngles != null && currentAngles.isValid())
            {
                switch (_stage)
                {
                    case 1:
                        {
                            if (_targetAzimuth.HasValue &&  Math.Abs(currentAngles.azimuth - _targetAzimuth.Value) <= _StageOneStep)
                            {
                                _cwait = _wait; //Досрочное завершение...
                            }
                            break;
                        }
                }
            }

            //Если _wait > 1, значит на отработку предыдущей инструкции требуется более одного цикла
            //Пака текущий счетчик _cwait меньше _wait обработка Do пропускается
            if (_cwait < _wait) 
            {
                _cwait++;
                Console.WriteLine($"ОБРАБОТКА: {_cwait}/{_wait}"); 
                return;
            }

            Console.WriteLine($"ПОИСК... этап {_stage}; dBm {signal}; азимут {_angles.azimuth}; элевация {_angles.placeAngle}");

            switch (_stage)
            {
                case 0: //Поворот антенны в первоначальное положение
                    {
                        SendAngles(_angles.azimuth, _angles.placeAngle, acm);

                        _wait = 10; //10 циклов на то, чтобы антенна установилась в нужное положение

                        NextStage();
                        break;
                    }
                case 1: //Первая стадия поиска +-10 градусов с шагом 1
                    {
                        _wait = 3;

                        if (signal < 0)
                        {
                            if (_bestSignal == 0 || _bestSignal < 0 && _bestSignal < signal)
                            {
                                FixSignal(signal);
                            }
                            else if (signal < _prevSignal && _prevSignal < _bestSignal)
                            {
                                Return(acm, false);
                                NextStage();
                                break;
                            }
                        }

                        _prevSignal = signal;

                        if (_path == null)
                        {
                            _path = InitPath(_angles.azimuth, _angles.placeAngle, _StageOneRange, _StageOneStep);
                            _pathIndex = 0;
                        }

                        if (_pathIndex < _path.Length)
                        {
                            Turn(acm);
                        }
                        else
                        {
                            Return(acm, false);
                            NextStage();
                        }

                        break;
                    }
                case 2: //Вторая стадия поиска +-2 градуса с шагом 0.2
                    {
                        _wait = 3;

                        if (signal < 0)
                        {
                            if (_bestSignal == 0 || _bestSignal < 0 && _bestSignal < signal)
                            {
                                FixSignal(signal);
                            }
                            else if (signal < _prevSignal && _prevSignal < _bestSignal)
                            {
                                NextStage();
                                break;
                            }
                        }

                        _prevSignal = signal;

                        if (_path == null)
                        {
                            _path = InitPath(_angles.azimuth, _angles.placeAngle, _StageTwoRange, _StageTwoStep);
                            _pathIndex = 0;
                        }

                        if (_pathIndex < _path.Length)
                        {
                            Turn(acm);
                        }
                        else
                        {
                            NextStage();
                        }

                        break;
                    }
                case 3: //Завершение поиска, возврат к наилучшему результату
                    {
                        Complete(acm);
                        break;
                    }
            }

            _cwait = 1;
        }

        /// <summary>
        /// Прерывание алгоритма поиска
        /// </summary>
        public void Break()
        {
            _completed = true;

            Console.WriteLine("ПОИСК ПРЕРВАН!");
        }


        /// <summary>
        /// Признак, завершен поиск или нет
        /// </summary>
        public bool isCompleted
        {
            get
            {
                return _completed;
            }
        }
        private bool _completed = false;







        //------------------
        //Внутренне API поиска
        //------------------


        private double[][] InitPath(double azimuth, double placeAngle, double range, double step)
        {
            var path = new List<double[]>();

            double minA = azimuth - range;
            double maxA = azimuth + range;
            double a = minA;

            while (a < maxA)
            {
                path.Add(new double[] { a, placeAngle });
                a += step;
            }

            return path.ToArray();
        }

        private void SendAngles(double azimuth, double placeAngle, IACMControl acm)
        {
            if (azimuth > 360)
                azimuth -= 360;

            if (azimuth < 0)
                azimuth += 360;

            if (placeAngle < 0)
                placeAngle = 0;

            Console.WriteLine($"ПОИСК -> ПОВОРОТ: азимут {azimuth}; элевация {placeAngle}");

            if (acm == null)
                return;

            _targetAzimuth = azimuth;
            _targetPlaceAngle = placeAngle;

            Task.Run(async () => { await acm.SendAngles(azimuth, placeAngle); });
        }

        private void FixSignal(double signal)
        {
            _bestSignal = signal;
            _bestAzimuth = _angles.azimuth;
            _bestPlaceAngle = _angles.placeAngle;

            Console.WriteLine($"ЛУЧШИЙ ВАРИАНТ: этап {_stage}; dBm {_bestSignal}; азимут {_bestAzimuth}; элевация {_bestPlaceAngle}");
        }
        private void NextStage()
        {
            _stage++;
            _bestSignal = _prevSignal = 0;
            _path = null;
        }

        private void Turn(IACMControl acm)
        {
            _angles.azimuth = _path[_pathIndex][0];
            _angles.placeAngle = _path[_pathIndex][1];

            SendAngles(_angles.azimuth, _angles.placeAngle, acm);

            _pathIndex++;
        }

        private void Return(IACMControl acm, bool success)
        {
            if (!_bestAzimuth.HasValue || !_bestPlaceAngle.HasValue)
                return;

            _angles.azimuth = _bestAzimuth.Value;
            _angles.placeAngle = _bestPlaceAngle.Value;  

            Console.WriteLine($"ПОИСК -> ВОЗВРАТ: азимут {_bestAzimuth}; элевация {_bestPlaceAngle}");
            SendAngles(_bestAzimuth.Value, _bestPlaceAngle.Value, acm);

            if (!success)
                return;

            OnSuccess?.Invoke(this, new SearchingResults() { azimuth = _bestAzimuth.Value, placeAngle = _bestPlaceAngle.Value });  
        }

        private void Complete(IACMControl acm)
        {
            _completed = true;

            Return(acm, true);

            Console.WriteLine("ПОИСК ЗАВЕРШЕН.");
        }
    }
}
