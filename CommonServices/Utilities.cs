﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;

/// <summary>
/// Вспомогательные функции
/// </summary>

public static class Utilities
{
    public static double ConvertToDouble(string text, double byDefault = 0)
    {
        if (string.IsNullOrWhiteSpace(text))
            return byDefault; 

        try
        {
            return Convert.ToDouble(text);
        }
        catch
        {
            try
            {
                if (text.IndexOf(".") != -1)
                    return Convert.ToDouble(text.Replace(".", ","));
                else if (text.IndexOf(",") != -1)
                    return Convert.ToDouble(text.Replace(",", "."));

                return byDefault;
            }
            catch  { }

            return byDefault;
        }
    }

    public static int ConvertToInt(string text, int byDefault = 0)
    {
        if (string.IsNullOrWhiteSpace(text))
            return byDefault;
        
        try
        {
            return Convert.ToInt32(text);
        }
        catch
        {
            return byDefault;
        }
    }

    public static DateTime ConvertToDateTime(string text)
    {
        if (string.IsNullOrWhiteSpace(text))
            return DateTime.MinValue;

        if (text.IndexOf(".") != -1)
            text = text.Replace(".", ":");

        try
        {
            return Convert.ToDateTime(text);
        }
        catch
        {
            return DateTime.MinValue;
        }
    }

    public static string ExceptionFullDescription(Exception ex)
    {
        if (ex.InnerException == null)
            return ex.Message;

        return string.Format("{0}\n{1}", ExceptionFullDescription(ex.InnerException), ex.Message);
    }

    public static void SetAppSettings(string key, string value)
    {
        try
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            if (settings[key] == null)
            {
                settings.Add(key, value);
            }
            else
            {
                settings[key].Value = value;
            }
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
        }
        catch (ConfigurationErrorsException)
        {
            Console.WriteLine("Error writing app settings");
        }
    }
}
