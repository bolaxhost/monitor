﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


//Описание структур данных, с которыми работает служба FlightService


/// <summary>
/// Данные модели: параметры телеметрии, уровень сигнала, положение антенны и т.д.
/// </summary>
namespace Model
{   
    /// <summary>
    /// Параметры базовой станции: местонахождение и углы
    /// </summary>
    public class FlightSettings
    {
        public Model.LocationModel BaseStation { get; set; }

        public double azimuthOffset { get; set; }
        public double placeAngleOffset { get; set; }

    }

    /// <summary>
    /// Базовый класс модели
    /// </summary>
    public abstract class BaseModel
    {
        public DateTime time;
        public bool isApproximated;

        public abstract bool isValid();

        public override string ToString()
        {
            return string.Format("T = {0}"
                , time.ToLongTimeString()
                );
        }
    }




    /// <summary>
    /// Данные телеметрии
    /// </summary>
    public class LocationModel : BaseModel 
    {
        public double lat;
        public double lon;
        public double pressure;
        public double height;

        public override bool isValid()
        {
            return lat > 0 && lon > 0 && pressure > 0;
        }

        public override string ToString()
        {
            return string.Format("{0}, Lat = {1}, Lon = {2}, P = {3}"
                , base.ToString()
                , lat
                , lon
                , pressure
                );
        }
    }



    /// <summary>
    /// Данные об уровне сигнала
    /// </summary>
    public class SignalModel : BaseModel
    {
        public double dBm;

        public override bool isValid()
        {
            return dBm < 0;
        }

        public override string ToString()
        {
            return string.Format("{0}, dBm = {1}"
                , base.ToString()
                , dBm
                );
        }
    }



    /// <summary>
    /// Данные о положении антенны
    /// </summary>
    public class AnglesModel : BaseModel
    {
        public double azimuth;
        public double placeAngle;

        public override bool isValid()
        {
            return placeAngle != 0;
        }

        public override string ToString()
        {
            return string.Format("{0}, Azm = {1}, El = {2}"
                , base.ToString()
                , azimuth
                , placeAngle
                );
        }
    }



    /// <summary>
    /// Результаты расчетов по данным телеметрии
    /// </summary>
    public class CalculatedByLocation
    {
        public double height;
        public double distance;
        public double azimuth;
        public double placeAngle;
        public double dBm;

        public override string ToString()
        {
            return string.Format("По данным бортовой телематики:\nВысота = {0} м\nРасстояние = {1} км\nАзимут = {2} градус\nЭлевация = {4} градус\nУровень сигнала {5} dBm"
                , height
                , distance
                , azimuth 
                , placeAngle
                , dBm
                );
        }
    }


    /// <summary>
    /// Результаты расчетов по уровню сигнала и углам
    /// </summary>
    public class CalculatedBySignalAndAngles
    {
        public LocationModel location = new LocationModel();
        public double height;
        public double distance;
        public override string ToString()
        {
            return string.Format("По данным БУА и ППМ:\nМестоположение = {0}\nВысота = {1} м\nРасстояние = {2} км"
                , location.ToString() 
                , height
                , distance
                );
        }
    }



    /// <summary>
    /// Текущее состояние FlightService 
    /// </summary>
    public class StateModel
    {
        //Момент времени
        public DateTime time = DateTime.Now;

        //Предыдущее состояние
        public StateModel prevState { get; set; }

        /// <summary>
        /// Основные параметры состояния: базовая станция, телеметрия от ЛА, уровень сигнала и углы антенны 
        /// </summary>
        public LocationModel station { get; set; }
        public LocationModel location { get; set; }
        public SignalModel signal { get; set; }
        public AnglesModel angles { get; set; }

        public StateModel()
        {

        }

        private StateModel(BaseModel item, StateModel prevState = null)
        {
            this.time = item.time;
            this.prevState = prevState;
        }

        public StateModel(SignalModel signal, StateModel prevState)
            : this((BaseModel)signal, prevState)
        {
            this.signal = signal;
        }

        //Представление состояния в виде строки
        public override string ToString()
        {
            return string.Format("Координаты:\n{0}\n\nСигнал:\n{1}\n\nУглы:\n{2}", location != null ? location.ToString() : "NULL", signal != null ? signal.ToString() : "NULL", angles != null ? angles.ToString() : "NULL");
        }


        /// <summary>
        /// Результаты расчетов
        /// </summary>
        public CalculatedByLocation resultsByLocation = new CalculatedByLocation();
        public CalculatedBySignalAndAngles resultsBySignalAndAngles = new CalculatedBySignalAndAngles();

        //Выполнение расчетов
        public void Calculate(LocationModel station)
        {
            this.station = station;

            if (this.prevState != null)
            {
                //"экстраполяция" координат
                if (this.location == null
                    && this.prevState.location != null
                    && this.prevState.location.isValid())
                {
                    var pl = this.prevState.location;

                    if (!pl.isApproximated
                        && this.prevState.prevState != null
                        && this.prevState.prevState.location != null
                        && this.prevState.prevState.location.isValid())
                    {
                        var ppl = this.prevState.prevState.location;

                        this.location = new LocationModel()
                        {
                            time = this.time,
                            lon = pl.lon * 2 - ppl.lon,
                            lat = pl.lat * 2 - ppl.lat,
                            pressure = (pl.pressure + ppl.pressure) / 2,
                            isApproximated = true,
                        };
                    }
                    else
                    {
                        this.location = new LocationModel()
                        {
                            time = this.time,
                            lon = pl.lon,
                            lat = pl.lat,
                            pressure = pl.pressure,
                            isApproximated = true,
                        };
                    }
                }

                //"экстраполяция" углов
                if (this.angles == null
                    && this.prevState.angles != null
                    && this.prevState.angles.isValid())
                {
                    this.angles = new AnglesModel()
                    {
                        time = this.time,
                        azimuth = this.prevState.angles.azimuth,
                        placeAngle = this.prevState.angles.placeAngle,
                        isApproximated = true,
                    };
                }

            }

            if (this.location != null && this.location.isValid())
                this.CalculateByLocation();

            if (this.signal != null && this.signal.isValid() && this.angles != null && this.angles.isValid())
                this.CalculateBySignalAndAngles();
        }

        private void CalculateByLocation()
        {
            //Высота
            GeoMath.CalculateHeight(station.pressure, station.height, location.pressure, ref resultsByLocation.height);

            //Азимут и расстояние
            GeoMath.CalculateDistanceAndAzimuth(station.lat, station.lon, location.lat, location.lon, ref resultsByLocation.azimuth, ref resultsByLocation.distance);

            //Угол места и уровень сигнала
            GeoMath.CalculatePlaceAngle(resultsByLocation.height, resultsByLocation.distance, ref resultsByLocation.placeAngle);

            //Уровень сигнала
            GeoMath.CalculateSignal(resultsByLocation.distance, ref resultsByLocation.dBm);

        }

        private void CalculateBySignalAndAngles()
        {
            GeoMath.CalculateLocationBySignalAngles(
                station.lat,
                station.lon,
                signal.dBm,
                angles.azimuth,
                angles.placeAngle,
                ref resultsBySignalAndAngles.location.lat,
                ref resultsBySignalAndAngles.location.lon,
                ref resultsBySignalAndAngles.distance,
                ref resultsBySignalAndAngles.height,
                ref resultsBySignalAndAngles.location.pressure);
        }







        /// <summary>
        /// Формирование отчета по состоянии в виде строки Excel в формате CSV
        /// </summary>
        /// <param name="addHeader"></param>
        /// <returns></returns>
        public List<string> MakeReport(bool addHeader)
        {
            string data = "";

            if (this.location != null)
            {
                data = data + string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}"
                    , time.ToLongTimeString()
                    , location.lat
                    , location.lon
                    , location.pressure
                    , resultsByLocation.height
                    , resultsByLocation.distance
                    , resultsByLocation.azimuth
                    , resultsByLocation.placeAngle
                    , resultsByLocation.dBm
                    );
            }
            else
            {
                data = data + ";;;;;;;;";
            }

            if (this.signal != null && this.angles != null)
            {
                data = data + string.Format(";{0};{1};{2};{3};{4}"
                    , resultsBySignalAndAngles.location.lat
                    , resultsBySignalAndAngles.location.lon
                    , resultsBySignalAndAngles.location.pressure
                    , resultsBySignalAndAngles.height
                    , resultsBySignalAndAngles.distance
                    );
            }
            else
            {
                data = data + ";;;;;";
            }

            if (this.angles != null)
            {
                data = data + string.Format(";{0};{1}"
                    , angles.azimuth
                    , angles.placeAngle
                    );
            }
            else
            {
                data = data + ";-;-";
            }

            if (this.signal != null)
            {
                data = data + string.Format(";{0}", signal.dBm);
            }
            else
            {
                data = data + ";-";
            }

            List<string> content = new List<string>() { data };

            if (addHeader)
            {
                content.Insert(0, "Время;Широта;Долгота;Давление;" +
                    "БТ Высота;БТ Расстояние [км];БТ Азимут;БТ Угол места;БТ Уровень сигнала;" +
                    "БУА и ППМ Широта;БУА и ППМ Долгота;БУА и ППМ Давление;БУА и ППМ Высота;БУА и ППМ Расстояние [км];" +
                    "Азимут;Угол места;dBm"
                    );
            }

            return content;
        }
    }
}
