﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonServices
{
    /// <summary>
    /// Интерфейс блока управления антенной
    /// </summary>
    public interface IACMControl
    {
        Task SendAngles(double azimuth, double placeAngle);
        Task Break();
        bool IsValid { get; }
    }
}
