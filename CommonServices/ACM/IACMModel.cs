﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonServices
{
    /// <summary>
    /// Интерйфейс состояния антенны
    /// </summary>
    public interface IACMModel
    {
        Task<Model.AnglesModel> Angles();
        bool IsValid { get; }
    }
}
