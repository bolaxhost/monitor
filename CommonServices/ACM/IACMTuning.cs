﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonServices
{
    /// <summary>
    /// Интерфейс наведения антенны по углам
    /// </summary>
    public interface IACMTuning
    {
        double AzimuthOffset { get; set; }

        double PlaceAngleOffset { get; set; }
    }
}
