﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Базовая математика

/// <summary>
/// Расчеты местоположения объекта по параметрам наведения и обратные расчеты
/// </summary>
public static class GeoMath
{
    /// <summary>
    /// Радиус земли
    /// </summary>
    public const double R = 6372.795;




    /// <summary>
    /// Расчет расстояния и азимута по координатам объекта и базовой станции
    /// </summary>
    /// <param name="stationLat">Широта базовой станции</param>
    /// <param name="stationLon">Долгота базовой станции</param>
    /// <param name="locationLat">Широта объекта</param>
    /// <param name="locationLon">Долгота объекта</param>
    /// <param name="azimuth">Вычисляемый азимут</param>
    /// <param name="distance">Вычисляемое расстояние</param>
    public static void CalculateDistanceAndAzimuth(double stationLat, double stationLon, double locationLat, double locationLon, ref double azimuth, ref double distance)
    {
        //Базовая станция
        double latA = stationLat * Math.PI / 180;
        double lonA = stationLon * Math.PI / 180;

        //ЛА
        double lat = locationLat * Math.PI / 180;
        double lon = locationLon * Math.PI / 180;

        double clA = Math.Cos(latA);
        double slA = Math.Sin(latA);

        double cl = Math.Cos(lat);
        double sl = Math.Sin(lat);

        double delta = lon - lonA;
        double deltaLat = lat - latA;

        double cdelta = Math.Cos(delta);
        double sdelta = Math.Sin(delta);

        //Вычисление расстояния
        double y = Math.Sqrt(Math.Pow(cl * sdelta, 2) + Math.Pow(clA * sl - slA * cl * cdelta, 2));
        double x = slA * sl + clA * cl * cdelta;
        double ad = Math.Atan(y / x);

        distance = ad * R;

        //Вычисление азимута
        x = (clA * sl) - (slA * cl * cdelta);
        y = sdelta * cl;

        //крайние условия
        if (delta == 0)
        {
            if (deltaLat > 0)
                azimuth = 0;
            else
                azimuth = 180;
        }
        else if (deltaLat == 0)
        {
            if (delta > 0)
                azimuth = 90;
            else
                azimuth = -90;
        }
        else //!=0
        {
            azimuth = Math.Atan(y / x) * 180 / Math.PI;

            if (deltaLat < 0)
            {
                if (delta > 0)
                    azimuth = azimuth + 180;
                else
                    azimuth = azimuth - 180;
            }
        }

        azimuth = azimuth % 360;

        //Перевод в ОДЗ 0-360
        if (azimuth < 0)
            azimuth += 360;
    }



    /// <summary>
    /// Расчет высоты по давлению в точке нахождения объекта
    /// </summary>
    /// <param name="p0">Давление в точке нахождения базовой станции</param>
    /// <param name="h0">Высота над уровнем моря базовой станции</param>
    /// <param name="p">Давление в точке нахождения объекта</param>
    /// <param name="h">Вычисляемая высота</param>
    public static void CalculateHeight(double p0, double h0, double p, ref double h)
    {
        h = (44330 - h0) * (1 - Math.Pow(p / p0, 1 / 5.255));
    }



    /// <summary>
    /// Расчет угла места (элевации) наведения антенны по высотае и расстоянию до объекта
    /// </summary>
    /// <param name="height">Высота объекта</param>
    /// <param name="distance">Расстояние до объекта</param>
    /// <param name="placeAngle">Вычислямый угол места</param>
    public static void CalculatePlaceAngle(double height, double distance, ref double placeAngle)
    {
        //Угол места
        if (distance > 0)
        {
            double beta = distance / R;
            double Rm = R * 1000;

            double x = (Rm + height) * Math.Sin(beta);
            double y = (Rm + height) * Math.Cos(beta) - Rm;

            placeAngle = Math.Atan(y / x) * 180 / Math.PI;
        }
        else
        {
            placeAngle = 90;
        }
    }




    /// <summary>
    /// Расчет теоретического значения уровня сигнала по расстоянию до объекта
    /// </summary>
    /// <param name="distance">Расстояние до объекта</param>
    /// <param name="dBm">Вычислямый уровень сигнала</param>
    public static void CalculateSignal(double distance, ref double dBm)
    {
        //Уровень сигнала
        dBm = -(34.66866686 + 20 * Math.Log10(distance));
    }




    /// <summary>
    /// Расчет высоты объекта по расстоянию и углу места наведения антенны (элевации)
    /// </summary>
    /// <param name="distance">Расстояние до объекта</param>
    /// <param name="placeAngle">Угол места</param>
    /// <param name="height">Вычисляемая высота</param>
    public static void CalculateHeightByDistanceAndPlaceAngle(double distance, double placeAngle, ref double height)
    {
        double paR = placeAngle * Math.PI / 180;
        double Rdx = R + distance * Math.Sin(paR);

        height = 1000 * ((Rdx) / Math.Cos(Math.Atan(distance * Math.Cos(paR) / (Rdx))) - R);
    }



    /// <summary>
    /// Расчет параметров цели по углам наведения и уровню сигнала
    /// </summary>
    /// <param name="stationLat">Широка базовой станции</param>
    /// <param name="stationLon">Долгота базовой станции</param>
    /// <param name="dBm">Уровень сигнала</param>
    /// <param name="azimuth">Азимут</param>
    /// <param name="placeAngle">Угол места (элевация)</param>
    /// <param name="locationLat">Вычисляемая широта объекта</param>
    /// <param name="locationLon">Вычисляемая долгота объекта</param>
    /// <param name="distance">Вычисляемая расстояние</param>
    /// <param name="height">Вычисляемая высота</param>
    /// <param name="pressure">Вычисляемая давление</param>
    public static void CalculateLocationBySignalAngles(double stationLat, double stationLon, double dBm, double azimuth, double placeAngle, ref double locationLat, ref double locationLon, ref double distance, ref double height, ref double pressure)
    {
        double latA = stationLat * Math.PI / 180;

        distance = 0.0185 * Math.Pow(10, -dBm / 20);

        double dR = distance / R;
        double azmR = azimuth * Math.PI / 180;
        double paR = placeAngle * Math.PI / 180;
        double Rdx = R + distance * Math.Sin(paR);

        locationLat = Math.Asin(Math.Sin(latA) * Math.Cos(dR) + Math.Cos(latA) * Math.Sin(dR) * Math.Cos(azmR)) * 180 / Math.PI;
        locationLon = stationLon + Math.Atan(Math.Sin(azmR) * Math.Sin(dR) / (Math.Cos(latA) * Math.Cos(dR) - Math.Sin(latA) * Math.Sin(dR) * Math.Cos(azmR))) * 180 / Math.PI;
        height = 1000 * ((Rdx) / Math.Cos(Math.Atan(distance * Math.Cos(paR) / (Rdx))) - R);
        pressure = Math.Pow(1 - height / 44330, 5.255) * 1013.3;
    }
}
