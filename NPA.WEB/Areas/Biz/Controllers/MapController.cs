﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text;
using System.Net;
using System.Web.Configuration;

using CommonServices;
using NPS.WEB.FlightServiceReference; 

using Newtonsoft.Json;

using NPS.WEB.Core;
using NPS.WEB.Areas.Models;

namespace NPS.WEB.Areas.Controllers
{
    [RoutePrefix("api/biz/map")]
    public class MapController : BaseSpatialController
    {
        [HttpGet]
        [Route("view")]
        public ActionResult Index()
        {
            return View("~/Areas/Biz/Views/Map/Index.cshtml");
        }

        [HttpGet]
        [Route("monitor-form")]
        public ActionResult EditForm()
        {
            return View("~/Areas/Biz/Views/Map/Monitor.cshtml");
        }

        [HttpGet]
        [Route("model")]
        public ActionResult GetModel()
        {
            try
            {
                return Json(new MapModel(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpGet]
        [Route("fsettings")]
        public ActionResult GetFlightSettings()
        {
            try
            {
                using (var flightService = new FlightServiceClient())
                {
                    var fsettings = JsonConvert.DeserializeObject<Model.FlightSettings>(flightService.GetData("settings"));
                    return Json(new
                    {
                        data = new
                        {

                            pressure = fsettings.BaseStation.pressure,
                            lat = fsettings.BaseStation.lat,
                            lon = fsettings.BaseStation.lon,
                            height = fsettings.BaseStation.height,
                            azimuthOffset = fsettings.azimuthOffset,
                            placeAngleOffset = fsettings.placeAngleOffset
                        }
                    },
                    JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("fsettings")]
        public ActionResult SetFlightSettings(double pressure, double lat, double lon, double height, double azimuthOffset, double placeAngleOffset)
        {
            try
            {
                using (var flightService = new FlightServiceClient())
                {
                    flightService.PostData("settings", JsonConvert.SerializeObject(new Model.FlightSettings()
                    {
                        BaseStation = new Model.LocationModel()
                        {
                            pressure = pressure,
                            lat = lat,
                            lon = lon,
                            height = height
                        },

                        azimuthOffset = azimuthOffset,
                        placeAngleOffset = placeAngleOffset,
                    }));
                }

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }


        [HttpGet]
        [Route("monitor")]
        public ActionResult GetMonitorData()
        {
            try
            {
                using (var flightService = new FlightServiceClient())
                {
                    return Json(new { state = flightService.GetState(), mode = flightService.GetMode() }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpGet]
        [Route("guidance")]
        public ActionResult GetGuidanceData(double lat, double lon, double placeAngle)
        {
            try
            {
                using (var flightService = new FlightServiceClient())
                {
                    var bsp = JsonConvert.DeserializeObject<Model.LocationModel>(flightService.GetData("base-station-parameters"));

                    double stationLat = bsp.lat;
                    double stationLon = bsp.lon;
                    double stationPressure = bsp.pressure;

                    double azimuth = 0;
                    double distance = 0;
                    double height = 0;

                    GeoMath.CalculateDistanceAndAzimuth(stationLat, stationLon, lat, lon, ref azimuth, ref distance);
                    GeoMath.CalculateHeightByDistanceAndPlaceAngle(distance, placeAngle, ref height);

                    return Json(new { data = new { azimuth = azimuth, distance = distance, height = height } }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("guidance")]
        public ActionResult DoManualGuidance(double azimuth, double placeAngle)
        {
            try
            {
                using (var flightService = new FlightServiceClient())
                {
                    flightService.Execute("guide", JsonConvert.SerializeObject(new Model.AnglesModel() { azimuth = azimuth, placeAngle = placeAngle }));
                }

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("base-station-pressure-set")]
        public ActionResult SetBaseStationPressure(double pressure)
        {
            try
            {
                using (var flightService = new FlightServiceClient())
                {
                    flightService.PostData("base-station-pressure-set", pressure.ToString());
                }

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("angles-tuning")]
        public ActionResult DoAnglesTuning(double azimuthOffset, double placeAngleOffset)
        {
            try
            {
                using (var flightService = new FlightServiceClient())
                {
                    flightService.PostData("azimuth-offset-add", azimuthOffset.ToString());
                    flightService.PostData("place-angle-offset-add", placeAngleOffset.ToString());
                }

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("break")]
        public ActionResult BreakManualGuidance()
        {
            try
            {
                using (var flightService = new FlightServiceClient())
                {
                    flightService.Execute("break", string.Empty);
                }

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("switch-mode")]
        public ActionResult SwitchMode()
        {
            try
            {
                using (var flightService = new FlightServiceClient())
                {
                    if (flightService.GetMode() == 0)
                        flightService.SetMode(1);
                    else
                        flightService.SetMode(0);
                }

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("searching")]
        public ActionResult StartSearching(double azimuth, double placeAngle)
        {
            try
            {
                using (var flightService = new FlightServiceClient())
                {
                    flightService.Execute("search", JsonConvert.SerializeObject(new Model.AnglesModel()
                    {
                        time = DateTime.Now,
                        azimuth = azimuth,
                        placeAngle = placeAngle,
                    }));
                }

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("commit")]
        public ActionResult PostWFS(string wfsUrl, string data)
        {
            try
            {
                PostWFSData(wfsUrl, data);

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpGet]
        [Route("data")]
        public ActionResult GetWFS(string wfsUrl, string layer, string filter = "")
        {
            try
            {
                return Json(new { data = GetWFSData(wfsUrl, layer, filter) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }
    }
}