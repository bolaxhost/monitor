﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace NPS.WEB.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return new RedirectResult("api/biz/map/view");
        }

        public ActionResult Error(HandleErrorInfo errorInfo)
        {
            return View("Error", errorInfo);
        }

        public ActionResult AccessDenied()
        {
            return View("AccessDenied");
        }
    }
}
