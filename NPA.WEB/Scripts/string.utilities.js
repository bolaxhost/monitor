﻿String.prototype.toStringArray = function (delimeter) {
    /// <summary>
    /// Функция преобразования строки в массив с обрезанием лишних пробелов в результирующем наборе строк.
    /// </summary>

    var valuesStr = this.split(delimeter);

    if (!valuesStr || valuesStr.length == 0) return null;

    var results = [];

    for (var index = 0; index < valuesStr.length; index++)
        results[index] = valuesStr[index].trim();

    return results;
};

String.prototype.format = function () {
    /// <summary>
    /// Функция форматирования строки.
    /// </summary>

    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {
        var regexp = new RegExp('\\{' + i + '\\}', 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};

String.prototype.reverse = function () {
    /// <summary>
    /// Функция переворачивания строки.
    /// </summary>

    return this.split("").reverse().join("");
};

String.prototype.trimAtStart = function (pattern) {
    var str = this;

    while (true) {
        var index = str.indexOf(pattern);
        if (index != 0) break;

        str = str.substring(pattern.length, str.length);
    }

    return str;
};

String.prototype.trimAtEnd = function (pattern) {
    var str = this.reverse();
    var result = str.trimAtStart(pattern.reverse());
    return result.reverse();
};

String.prototype.trimWith = function (trimStr) {
    /// <summary>
    /// Функция удаления указанной последовательности символом из начала и конца строки.
    /// </summary>

    var value = this.trim();
    trimStr = trimStr || "";

    if (value.length == 0 || trimStr.length == 0)
        return value;

    value = value.trimAtStart(trimStr);
    value = value.trimAtEnd(trimStr);

    return value;
};

String.prototype.allIndexes = function (lookFor) {
    /// <summary>
    /// Функция возвращает массив индексов всех вхождений подстроки в строку.
    /// </summary>
    var indices = new Array();
    var i = 0;
    var index = this.indexOf(lookFor, 0);
    while (index >= 0) {
        indices[i] = index;
        i++;
        index = this.indexOf(lookFor, index + 1);
    }
    return indices;
};

String.prototype.startsWith = function (suffix) {
    /// <summary>
    /// Заканчивается ли строка suffix-ом.
    /// </summary>
    return this.indexOf(suffix) == 0;
};

String.prototype.endsWith = function (suffix) {
    /// <summary>
    /// Заканчивается ли строка suffix-ом.
    /// </summary>
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

String.prototype.insert = function (index, string) {
    /// <summary>
    /// Вставить подстроку в строку.
    /// </summary>
    if (index < 0)
        index = this.length + index;
    if (index > 0)
        return this.substring(0, index) + string + this.substring(index, this.length);
    else
        return string + this;
};

String.prototype.contains = function (index) {
    return this.indexOf(index) !== -1;
}