﻿//Проверка, является ли браузер IE
function isIE() {
    return (("Microsoft Internet Explorer" == navigator.appName) || // IE < 11
        (null != navigator.userAgent.match(/Trident\/./i))); // IE11
}

//Проверка, установлен ли plugIn КриптоПро ЭЦП
function isPluginInstalled() {
    if (isIE()) {
        try {
            var obj = new ActiveXObject("CAdESCOM.CPSigner");
            return true;
        }
        catch (err) { }
    }
    else {
        var mimetype = navigator.mimeTypes["application/x-cades"];
        if (mimetype) {
            var plugin = mimetype.enabledPlugin;
            if (plugin) {
                return true;
            }
        }
    }
    return false;
}

function ConvertToDate(date) {
    if (isIE()) {
        return date.getVarDate();
    }

    return date;
}

//Возвращает объекты КриптоПро, в зависимости от браузера
function CreateObject(name) {
    var userAgent = navigator.userAgent;
    switch (isIE()) {
        case true:
            return new ActiveXObject(name);
        default:
            if (!isPluginInstalled()) {
                //Браузер не IE и нет плагина
                alert("Не установлен КриптоПро ЭЦП Browser plug-in.");
                return null;
            }
            
            if (userAgent.match(/ipod/i) || userAgent.match(/ipad/i) || userAgent.match(/iphone/i)) {
                return call_ru_cryptopro_npcades_10_native_bridge("CreateObject", [name]);
            }
            var cadesobject = document.getElementById("cadesplugin");
            return cadesobject.CreateObject(name);
    }
}

// Формирует список сертификатов пользователя
function EnumAvailableCertificates(listId) {
    EnumAvailableCertificateList(listId, false, null, null);
}

function EnumAvailableCertificateList(certList, preserve, singleSigner, certThumbprint) {
    try {
        var MyStore = CreateObject("CAPICOM.Store");
    }
    catch (e) {
        alert("Не установлен объект CAPICOM: " + e.toString());
        return false;
    }
    try {
        MyStore.Open(CAPICOM_CURRENT_USER_STORE, "My", CAPICOM_STORE_OPEN_READ_ONLY);
    }
    catch (e) {
        if (e.number != CAPICOM_E_CANCELLED) {
            alert("Ошибка при открытии персонального хранилища сертификатов");
            return false;
        }
        else { return false; }
    }

    if (typeof (certList).toLowerCase() == "string")
        certList = document.getElementById(certList);

    if (!preserve && certList && certList.innerHTML)
        certList.innerHTML = "";
    var FilteredCertificates = MyStore.Certificates.Find(CAPICOM_CERTIFICATE_FIND_KEY_USAGE, CAPICOM_DIGITAL_SIGNATURE_KEY_USAGE).Find(CAPICOM_CERTIFICATE_FIND_TIME_VALID);
    var oOption;
    if (FilteredCertificates.Count > 0) {
        var sFieldsList;
        var aFields = new Array();
        for (i = 1; i <= FilteredCertificates.Count; i++) {
            var signername = FilteredCertificates.Item(i).GetInfo(CAPICOM_CERT_INFO_SUBJECT_SIMPLE_NAME);

            if (certThumbprint == null) {
                //string.substring(from, to)
                var checkName = signername.split(' ').join('').toLowerCase();
                var pos = checkName.indexOf(',', 0);
                if (pos > -1)
                    checkName = checkName.substring(0, pos);

                // устанение проблемы с "е" и "ё" (все Ё заменяем на Е)
                if (checkName != null)
                    checkName = checkName.replace("ё", "е").replace("Ё", "Е");
                if (singleSigner != null)
                    singleSigner = singleSigner.replace("ё", "е").replace("Ё", "Е");
                //--

                if (singleSigner != null && TrimStr(checkName) != TrimStr(singleSigner.split(' ').join('')).toLowerCase()) continue;

                oOption = document.createElement("OPTION");
                certList.add(oOption);
                oOption.innerHTML = signername + ' (до:' + FilteredCertificates.Item(i).ValidToDate + ')';
                oOption.value = FilteredCertificates.Item(i).Thumbprint;
            }
            else {
                if (FilteredCertificates.Item(i).Thumbprint == certThumbprint) {
                    oOption = document.createElement("OPTION");
                    certList.add(oOption);
                    oOption.innerHTML = signername + ' (до:' + FilteredCertificates.Item(i).ValidToDate + ')';
                    oOption.value = FilteredCertificates.Item(i).Thumbprint;
                }
            }
        }
    }
    else {

        alert("Не найдено ни одного действующего сертификата для ЭЦП");
        return false;
    }

    // Clean Up
    MyStore = null;
    FilteredCertificates = null;
}

// find cert by hash
function FindCertificateByHash(szThumbprint) {
    // instantiate the CAPICOM objects
    try {
        var MyStore = CreateObject("CAPICOM.Store");
    }
    catch (e) {
        alert("Не установлен объект CAPICOM");
        return null;
    }
    // open the current users personal certificate store
    try {
        MyStore.Open(CAPICOM_CURRENT_USER_STORE, "My", CAPICOM_STORE_OPEN_READ_ONLY);
    }
    catch (e) {
        if (e.number != CAPICOM_E_CANCELLED) {
            alert("Ошибка при открытии персонального хранилища сертификатов.");
            return null;
        }
    }

    // find all of the certificates that have the specified hash
    var FilteredCertificates = MyStore.Certificates.Find(CAPICOM_CERTIFICATE_FIND_SHA1_HASH, szThumbprint);
    if (FilteredCertificates.Count > 0) {
        return FilteredCertificates.Item(1);
    }
    else {
        alert("Ошибка. Не найден сертификат пользователя.");
        return null;
    }

    MyStore = null;
    FilteredCertificates = null;
}

function SignDataCapicom(sData, sCertHash) {
    try {
        var SignedData = CreateObject("CAPICOM.SignedData");
        var Signer = CreateObject("CAPICOM.Signer");
        var TimeAttribute = CreateObject("CAPICOM.Attribute");
    }
    catch (e) {
        alert("Не установлен объект CAPICOM ");
        return false;
    }

    if (sCertHash != "") {
        // Set the data that we want to sign
        SignedData.Content = sData;
        try {
            // Set the Certificate we would like to sign with
            Signer.Certificate = FindCertificateByHash(sCertHash);
            if (Signer.Certificate == null)
                return false;
            // Set the time in which we are applying the signature
            var Today = new Date();
            TimeAttribute.Name = CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME;
            TimeAttribute.Value = ConvertToDate(Today);
            Today = null;
            Signer.AuthenticatedAttributes.Add(TimeAttribute);

            // Do the Sign operation
            var szSignature = SignedData.Sign(Signer, true, CAPICOM_ENCODE_BASE64);

            return szSignature;
        }
        catch (e) {
            if (e.number != CAPICOM_E_CANCELLED) {
                alert("Ошибка при подписании данных: " + e.description);
                return false;
            }
            else { return false; }
        }
    }
}

function SignDataCadescom(sData, sCertHash) {
    try {
        var SignedData = CreateObject("CAdESCOM.CadesSignedData");
        var Signer = CreateObject("CAdESCOM.CPSigner");
        var TimeAttribute = CreateObject("CAdESCOM.CPAttribute");
    }
    catch (e) {
        alert("Не установлен объект CAdESCOM ");
        return false;
    }

    if (sCertHash != "") {
        // Set the data that we want to sign
        SignedData.Content = sData;
        try {
            // Set the Certificate we would like to sign with
            Signer.Certificate = FindCertificateByHash(sCertHash);
            if (Signer.Certificate == null)
                return false;
            // Set the time in which we are applying the signature
            var Today = new Date();
            TimeAttribute.Name = CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME;
            TimeAttribute.Value = ConvertToDate(Today);
            Today = null;
            Signer.AuthenticatedAttributes2.Add(TimeAttribute);

            // Do the Sign operation
            var szSignature = SignedData.SignCades(Signer, CADESCOM_CADES_BES, true, CAPICOM_ENCODE_BASE64);

            return szSignature;
        }
        catch (e) {
            if (e.number != CAPICOM_E_CANCELLED) {
                alert("Ошибка при подписании данных: " + e.message);
                return false;
            }
            else { return false; }
        }
    }
}

function SignData(sData, sCertHash) {
    if (!isIE() || isPluginInstalled()) {
        return SignDataCadescom(sData, sCertHash);
    }

    if (isIE())
        return SignDataCapicom(sData, sCertHash);
}

function SignForm(dataValueId, certListId, signValueId) {
    var sDataCtrl = $("#" + dataValueId);
    var signCtrl = $("#" + signValueId);

    var sData;
    if (sDataCtrl.get(0).nodeName.toLowerCase() == 'input')
        sData = sDataCtrl.val();
    else
        sData = sDataCtrl.html();

    if (sData == null || sData == "") {
        alert("Не задано содержание для подписания!");
        return false;
    }

    var thumb = $("#" + certListId).val();
    try {
        var sSignature = SignData(sData, thumb);
        if (sSignature == null || sSignature == false) {
            sSignature = "";
        }
        if (signCtrl.get(0).nodeName.toLowerCase() == 'input') {
            signCtrl.val(sSignature);
        }
        else {
            signCtrl.html(sSignature);
        }
        return true;
    }
    catch (e) { alert(e); alert("Произошла ошибка!"); return false; }
}
