﻿'use strict';

(function () {
    angular
        .module('NpsAppConfig', [
            'ngRoute',
            'ngResource',
            'ngCookies',
            'ngSanitize'
        ])
        .config([
            '$routeProvider', '$locationProvider', '$httpProvider', '$compileProvider', AppConfig
        ])

    //Базовая конфигурация приложения
    function AppConfig($routeProvider, $locationProvider, $httpProvider, $compileProvider) {
        $compileProvider.aHrefSanitizationWhitelist(/#/);
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?):/);
        $locationProvider.html5Mode(false);
    }
})()
