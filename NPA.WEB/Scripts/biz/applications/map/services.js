﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .factory('BizControllerHelper', ['CommonControllerApi',
        function (CommonControllerApi) {
            return new CommonControllerApi('/monitor');
        }]);
})();