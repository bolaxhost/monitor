﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .controller('MonitorCtrl', ['$scope', '$rootScope', '$resource', '$route', '$sce', '$routeParams', '$location', MonitorController]);

    function MonitorController($scope, $rootScope, $resource, $route, $sce, $routeParams, $location) {
        //Загрузка настроек редактора
        var resource = $resource('/api/biz/map/model', {}, {
            'getModel': { method: 'GET' },
        });



        $scope.toggleBlock = function (name) {
            $scope["_block_visible_" + name] = !$scope["_block_visible_" + name];
        }

        $scope.isVisibleBlock = function (name) {
            return $scope["_block_visible_" + name];
        }

        $scope.toggleBlock('BT');
        $scope.toggleBlock('BUA');
        $scope.toggleBlock('PPM');


        //Начальные параметры модели
        $scope.data = resource.getModel();




        //Центр карты и масштаб по умолчанию
        var defaultCenter = [4187136.7833, 7509542.7662];
        var defaultZoom = 8;

        //Отложенная загрузка карты
        window.setTimeout(function () {
            $scope.initializeMap();      //#1
            $scope.finalMapInitialing(); //#2
        }, 1000);

        //Первоначальная загрузка карты
        $scope.initializeMap = function () {
            $scope.mapData = { layers: {}, sources: {} };




            //Настройки отображения
            //Трек и ЛА
            $scope.planeStyles = new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(0, 128, 0, 0.5)'
                }),
                stroke: new ol.style.Stroke({
                    color: 'rgba(0, 128, 128, 1)',
                    width: 4,
                    lineDash: [.1, 5]
                }),
                image: new ol.style.Circle({
                    radius: 5,
                    fill: new ol.style.Fill({
                        color: 'rgba(255, 0, 0, 0.5)'
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'rgba(255, 0, 0, 1)',
                        width: 1
                    })
                })
            })

            //Станция
            $scope.stationStyles = new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 7,
                    fill: new ol.style.Fill({
                        color: 'rgba(0, 255, 255, 0.5)'
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'rgba(255, 0, 0, 1)',
                        width: 1
                    })
                })
            })

            //Точка ручного наведения
            $scope.guidanceStyles = new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 5,
                    fill: new ol.style.Fill({
                        color: 'rgba(255, 0, 255, 0.5)'
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'rgba(255, 0, 0, 1)',
                        width: 1
                    })
                })
            })

            //Слои и источники
            //Трек и ЛА
            $scope.mapData.sources.plane = new ol.source.Vector({ wrapX: false });
            $scope.mapData.layers.plane = new ol.layer.Vector({
                source: $scope.mapData.sources.plane,
                style: $scope.planeStyles,
            });

            //Станция
            $scope.mapData.sources.station = new ol.source.Vector({ wrapX: false });
            $scope.mapData.layers.station = new ol.layer.Vector({
                source: $scope.mapData.sources.station,
                style: $scope.stationStyles,
            });

            //Ручное наведение
            $scope.mapData.sources.guidance = new ol.source.Vector({ wrapX: false });
            $scope.mapData.layers.guidance = new ol.layer.Vector({
                source: $scope.mapData.sources.guidance,
                style: $scope.guidanceStyles,
            });

            //Обработка добавления точки - цели ручного наведения
            $scope.mapData.sources.guidance.on("change", function (e) {
                var features = $scope.mapData.sources.guidance.getFeatures();
                if (features && features.length > 0) {
                    var projPoint = features[0].getGeometry().getCoordinates();
                    var realPoint = ol.proj.transform(projPoint, 'EPSG:3857', 'EPSG:4326');

                    $scope.guidance.lon = Number(realPoint[0].toFixed(4));
                    $scope.guidance.lat = Number(realPoint[1].toFixed(4));

                    $scope.mapData.sources.guidance.clear();
                    $scope.mapData.map.removeInteraction($scope.mapData.drawGuidanceInteraction);

                    //Переход на этап определения параметров...
                    $scope.manualGuidanceStage = 2;
                }
            });

            //Универсальная функция для чтения данных через WFS
            var loadData = function (extent, resolution, projection, layerName, source) {
                var resource = $resource('/api/biz/map/data', {}, {
                    'getData': { method: 'GET' },
                });

                resource.getData({ layer: layerName, filter: "&SRSNAME=urn:x-ogc:def:crs:EPSG:3857&BBOX=" + extent.join(",") + ",EPSG:3857" }).$promise.then(function (response) {
                    source.addFeatures(new ol.format.GeoJSON().readFeatures(JSON.parse(response.data)));
                });
            }


            //Проекция
            var mapProjection = new ol.proj.Projection({
                code: 'EPSG:3857',
                units: 'm'
            });


            //Представление
            $scope.mapData.view = new ol.View({
                center: defaultCenter,
                zoom: defaultZoom,
                maxZoom: 21,
                projection: mapProjection
            });

            //Интерактив для нанесения точки - цели ручного наведения
            $scope.mapData.drawGuidanceInteraction = new ol.interaction.Draw({
                source: $scope.mapData.sources.guidance,
                type: 'Point',
            });




            //Список слоев и групп
            var layers = [
                    new ol.layer.Tile({
                        source: new ol.source.OSM()
                    }),
                    $scope.mapData.layers.plane,
                    $scope.mapData.layers.station,
                    $scope.mapData.layers.guidance,
            ];

            $scope.mapData.layersGroup = new ol.layer.Group({
                layers: layers,
            });





            //Инициализация карты openlayers
            $scope.mapData.map = new ol.Map({
                target: 'map',
                view: $scope.mapData.view
            });






            //Добавление слоев и завершение инициализации
            $scope.mapData.map.setLayerGroup($scope.mapData.layersGroup);
            $scope.mapIsInitialized = true;
        }




        //Восстановление сохраненных параметров карты
        $scope.finalMapInitialing = function () {
            //Получение данных монитора
            var resource = $resource('/api/biz/map/monitor', {}, {
                'getMonitorData': { method: 'GET' },
            });

            //Режим навигации
            $scope.follow = true;
            $scope.switchFollow = function () {
                $scope.follow = !$scope.follow;
            }


            //Поиск
            $scope.startSearching = function () {
                var azimuth = $scope.guidance.azimuth;
                var placeAngle = $scope.guidance.placeAngle;

                var resource = $resource('/api/biz/map/searching', {}, {
                    'searching': { method: 'POST' },
                });

                resource.searching({ azimuth: azimuth, placeAngle: placeAngle }).$promise.then(function (response) {
                    if (response.state == "error") {
                        alert(response.message);
                        return;
                    }

                    console.log("Команда поиска сигнала отправлена успешно.");
                    $scope.stopManualGuidance();
                });
            }

            //Параметры среды
            $scope.fsettings = {
                pressure: 0,
                lon: 0,
                lat: 0,
                height: 0,
                azimuthOffset: 0,
                placeAngleOffset: 0,
            }
            $scope.openFlightSettingsDialog = function () {
                var resource = $resource('/api/biz/map/fsettings', {}, {
                    'fsettings': { method: 'GET' },
                });

                resource.fsettings({}).$promise.then(function (response) {
                    if (response.state == "error") {
                        alert(response.message);
                        return;
                    }

                    $scope.fsettings = response.data;
                    $scope.editFSettingsMode = 1;
                });

            }
            $scope.closeFlightSettingsDialog = function () {
                $scope.editFSettingsMode = 0;
            }
            $scope.saveFlightSettings = function () {
                var resource = $resource('/api/biz/map/fsettings', {}, {
                    'saveFSettings': { method: 'POST' },
                });

                resource.saveFSettings(
                    {
                        pressure: $scope.fsettings.pressure,
                        lon: $scope.fsettings.lon,
                        lat: $scope.fsettings.lat,
                        height: $scope.fsettings.height,
                        azimuthOffset: $scope.fsettings.azimuthOffset,
                        placeAngleOffset: $scope.fsettings.placeAngleOffset,
                    }).$promise
                    .then(function (response) {
                    if (response.state == "error") {
                        alert(response.message);
                        return;
                    }

                    console.log("Параметры полета сохранены успешно.");
                });

                $scope.closeFlightSettingsDialog();
            }
            $scope.toggleEditPressure = function () {
                if (!$scope.editFSettingsMode) {
                    $scope.editFSettingsMode = 2;
                } else {
                    if ($scope.editFSettingsMode == 2) {
                        $scope.editFSettingsMode = 0;
                    }
                }
            }
            $scope.postPressure = function () {
                var resource = $resource('/api/biz/map/base-station-pressure-set', {}, {
                    'setBaseStationPressure': { method: 'POST' },
                });

                resource.setBaseStationPressure({ pressure: $scope.fsettings.pressure }).$promise.then(function (response) {
                    if (response.state == "error") {
                        alert(response.message);
                        return;
                    }

                    console.log("Значение давления на станции слежения обновлено.");
                });
                $scope.toggleEditPressure();
            }
            $scope.applyAnglesOffset = function (ao, pao) {
                var resource = $resource('/api/biz/map/angles-tuning', {}, {
                    'setAnglesOffset': { method: 'POST' },
                });

                resource.setAnglesOffset({ azimuthOffset: ao, placeAngleOffset: pao }).$promise.then(function (response) {
                    if (response.state == "error") {
                        alert(response.message);
                        return;
                    }

                    console.log("Поправки по углам приняты.");
                });
            }

            //Ручное наведение
            //Параметры
            $scope.manualGuidanceStage = 0;
            $scope.guidance = {
                placeAngle: 3,
                azimuth: 0,
                lat: 0,
                lon: 0,
                distance: 0,
                height: 0,
                precision: 0.1,
                editAngles: false,
            };
            //Этапы
            //#0=>#1
            $scope.startManualGuidance = function () {
                $scope.mapData.map.addInteraction($scope.mapData.drawGuidanceInteraction);
                $scope.manualGuidanceStage = 1;
                $scope.guidance.editAngles = false;
            }
            //Наведение по углам
            $scope.startManualGuidanceByAngles = function () {
                if ($scope.lastMonitorData) {
                    var source = $scope.lastMonitorData.angles || $scope.lastMonitorData.resultsByLocation;
                    if (source) {
                        $scope.guidance.azimuth = source.azimuth;
                        $scope.guidance.placeAngle = source.placeAngle;
                    }
                }

                $scope.manualGuidanceStage = 4;
                $scope.guidance.editAngles = true;
                $scope.syncGuidanceAngles = true;
            }
            //#2=>#3
            $scope.calculateGuidanceEnvironment = function () {
                var resource = $resource('/api/biz/map/guidance', {}, {
                    'getData': { method: 'GET' },
                });

                resource.getData({ lat: $scope.guidance.lat, lon: $scope.guidance.lon, placeAngle: $scope.guidance.placeAngle }).$promise.then(function (response) {
                    if (response.state == "error") {
                        alert(response.message);
                        return;
                    }

                    $scope.guidance.azimuth = response.data.azimuth;
                    $scope.guidance.distance = response.data.distance;
                    $scope.guidance.height = response.data.height;

                    $scope.manualGuidanceStage = 3;
                });
            }
            //#3->#4
            $scope.doManualGuidance = function () {
                var resource = $resource('/api/biz/map/guidance', {}, {
                    'guidance': { method: 'POST' },
                });

                resource.guidance({ azimuth: $scope.guidance.azimuth, placeAngle: $scope.guidance.placeAngle }).$promise.then(function (response) {
                    if (response.state == "error") {
                        alert(response.message);
                        return;
                    }

                    console.log("Команда ручного наведения по азимуту " + $scope.guidance.azimuth + " и углу места " + $scope.guidance.placeAngle + " отправлена успешно.");
                    $scope.manualGuidanceStage = 4;
                });
            }
            //
            $scope.doManualGuidanceAndTune = function () {
                $scope.doManualGuidance();

                $scope.guidance.editAngles = false;
                $scope.syncGuidanceAngles = true;
            }
            //Break
            $scope.breakManualGuidance = function () {
                var resource = $resource('/api/biz/map/break', {}, {
                    'break': { method: 'POST' },
                });

                resource.break({ }).$promise.then(function (response) {
                    if (response.state == "error") {
                        alert(response.message);
                        return;
                    }

                    console.log("Команда наведения отменена.");
                    $scope.syncGuidanceAngles = true;
                });
            }
            //#...
            $scope.stopManualGuidance = function () {
                if ($scope.manualGuidanceStage == 1) {
                    $scope.mapData.map.removeInteraction($scope.mapData.drawGuidanceInteraction);
                }

                $scope.manualGuidanceStage = 0;
                delete $scope.syncGuidanceAngles;
            }

            //Ручное наведение антенны
            $scope.moveRight = function () {
                $scope.guidance.azimuth += $scope.guidance.precision;
                if ($scope.guidance.azimuth > 360)
                    $scope.guidance.azimuth -= 360;

                $scope.doManualGuidance();
                delete $scope.syncGuidanceAngles
            }
            $scope.moveLeft = function () {
                $scope.guidance.azimuth -= $scope.guidance.precision;
                if ($scope.guidance.azimuth < 0)
                    $scope.guidance.azimuth += 360;

                $scope.doManualGuidance();
                delete $scope.syncGuidanceAngles
            }
            $scope.moveUp = function () {
                $scope.guidance.placeAngle += $scope.guidance.precision;
                $scope.doManualGuidance();
                delete $scope.syncGuidanceAngles
            }
            $scope.moveDown = function () {
                $scope.guidance.placeAngle -= $scope.guidance.precision;
                $scope.doManualGuidance();
                delete $scope.syncGuidanceAngles
            }

            //Получение и обработка данных от flightService
            $scope.track = [];
            $scope.lastMonitorData = null;
            $scope.repeatCount = 0;
            $scope.dataWarning = false;
            $scope.scanMonitor = function () {
                resource.getMonitorData().$promise.then(function (response) {
                    try {
                        var monitorData = JSON.parse(response.state);
                        monitorData.mode = parseInt(response.mode);
                    }
                    catch (e) {
                        return;
                    }
                    
                    monitorData.time = new Date(monitorData.time);

                    if ($scope.lastMonitorData && $scope.lastMonitorData.time.toTimeString() == monitorData.time.toTimeString()) {
                        $scope.repeatCount++;

                        if ($scope.repeatCount > 2) {
                            $scope.dataWarning = true;
                        }

                        return;
                    }

                    $scope.repeatCount = 0;
                    $scope.dataWarning = false;

                    //Данные БТ
                    var location = monitorData.location;

                    //Расчетные данные
                    if (!location && monitorData.signal && monitorData.angles) {
                        location = monitorData.resultsBySignalAndAngles.location;
                    }

                    //Визуализация ЛА
                    if (location) {
                        if (!$scope.lastMonitorData || !$scope.lastMonitorData.location || ($scope.lastMonitorData.location.lat != location.lat || $scope.lastMonitorData.location.lon != location.lon)) {
                            var coord = ol.proj.transform([location.lon, location.lat], 'EPSG:4326', 'EPSG:3857');
                            $scope.track.push(coord);

                            $scope.mapData.sources.plane.clear();
                            $scope.mapData.sources.plane.addFeature(new ol.Feature({ geometry: new ol.geom.Point(coord) }));

                            if ($scope.track.length > 1) {
                                $scope.mapData.sources.plane.addFeature(new ol.Feature({ geometry: new ol.geom.LineString($scope.track) }));
                            }

                            if ($scope.follow) {
                                $scope.mapData.map.getView().setCenter(coord);
                            }
                        }
                    }

                    //Визуализация станции
                    if (monitorData.station) {
                        if (!$scope.lastMonitorData || !$scope.lastMonitorData.station || ($scope.lastMonitorData.station.lat != monitorData.station.lat || $scope.lastMonitorData.station.lon != monitorData.station.lon)) {
                            var coord = ol.proj.transform([monitorData.station.lon, monitorData.station.lat], 'EPSG:4326', 'EPSG:3857');

                            $scope.mapData.sources.station.clear();
                            $scope.mapData.sources.station.addFeature(new ol.Feature({ geometry: new ol.geom.Point(coord) }));
                        }
                    }

                    //syncGuidanceAngles
                    if ($scope.syncGuidanceAngles && monitorData.angles && !$scope.guidance.editAngles) {
                        $scope.guidance.azimuth = monitorData.angles.azimuth;
                        $scope.guidance.placeAngle = monitorData.angles.placeAngle;
                    }

                    $scope.lastMonitorData = monitorData;
                    $scope.lastMonitorData.timeString = $scope.lastMonitorData.time.toLocaleTimeString('ru-ru', { hour12: false });
                    $scope.lastMonitorData.modeName = ($scope.lastMonitorData.mode == 0) ? "Автосопровождение" : "Целеуказание";

                    if (!$scope.editFSettingsMode && $scope.lastMonitorData.station) {
                        $scope.fsettings.pressure = $scope.lastMonitorData.station.pressure;
                        $scope.fsettings.lat = $scope.lastMonitorData.station.lat;
                        $scope.fsettings.lon = $scope.lastMonitorData.station.lon;
                        $scope.fsettings.height = $scope.lastMonitorData.station.height;
                    }

                });
            }

            //Смена режима: автосопровождение/целеуказание
            $scope.switchMode = function () {
                if (!$scope.lastMonitorData)
                    return;

                if ($scope.lastMonitorData.mode == 0) {
                    if (!$scope.lastMonitorData.location) {
                        alert("Переключение в режим целеуказания невозможно при отсутствии данных БТ!");
                        return;
                    }
                }

                $resource('/api/biz/map/switch-mode', {}, {
                    'switchMode': { method: 'POST' },
                }).switchMode().$promise.then(function () { $scope.lastMonitorData.mode = -1; });
            }

            //Запуск сканирования
            setInterval($scope.scanMonitor, 1000)
        }
    }
})();