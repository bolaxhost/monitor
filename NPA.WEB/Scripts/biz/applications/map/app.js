﻿'use strict';

(function () {
    angular
        .module('BizApp', [
            'NpsAppConfig',
            'NpsDbApi',
            'NpsControllerHelpers',
            'NpsDirectives'
    ])
        .config([
            '$routeProvider', '$locationProvider', '$httpProvider', '$compileProvider', AppConfig
        ])
        .run([
            '$rootScope', '$window', '$document', '$location', AppRun
        ]);

    //Конфигурация
    function AppConfig($routeProvider, $locationProvider, $httpProvider, $compileProvider) {
        $routeProvider.
              when('/monitor', {
                  templateUrl: '/api/biz/map/monitor-form',
                  controller: 'MonitorCtrl',
              }).
              otherwise({
                  redirectTo: '/monitor'
              });
    }

    //Старт
    function AppRun($rootScope, $window, $document, $location) {
    }
})();