function ds_submit(formId) {
    var AllForm = Boolean(formId == undefined || formId == "");

    if (AllForm) {
        $('#ds_DataToSign').val(ds_getAllDataToSign());
    } else {
        $('#ds_DataToSign').val(ds_getFormDataToSign(formId));
    }

    if (!SignForm('ds_DataToSign', 'ds_Certificate', 'ds_Signature'))
        return false;

    if (AllForm) {
        $('form').submit();
    } else {
        $('#' + formId).submit();
    }

    return true;
}

function ds_getAllDataToSign() {
    var report = new Object();

    $("form").each(function (index) {
        report["form_" + index] = ds_createForm(this);
    })

    return JSON.stringify(report, null, "\t");
}

function ds_getFormDataToSign(formId) {
    return JSON.stringify(ds_createForm($("#" + formId)), null, "\t");
}

function ds_createForm(form){
    var result = new Object();
    result.id = $(form).attr("id");

    $("input[type!='hidden'][type!='reset'][type!='submit'][type!='button']", form).each(function () {
        var id = $(this).attr("id");
        result["input_" + id] = ds_createInput(form, id, $(this).attr("name"), $(this).attr("value"));
    });

    $("textarea", form).each(function () {
        var id = $(this).attr("id");
        result["input_" + id] = ds_createInput(form, id, $(this).attr("name"), $(this).html());
    });

    $(".fileProperty", form).each(function () {
        var id = $(this).attr("fileId");
        result["file_" + id] = ds_createFile($(this), form);
    });

    return result;
}

function ds_createInput(form, id, name, value) {
    var input = new Object();
    input.id = id;
    input.name = name;
    input.value = value;

    var label = $("label[for=" + input.id + "]", form)
    if (label != undefined) {
        input.description = label.html();
    }

    return input;
}

function ds_createFile(value, form) {
    var file = new Object();
    file.fileId = $(value).attr("fileId");
    file.fileName = $(value).attr("fileName");
    file.fileSize = $(value).attr("fileSize");
    file.fileHash = $(value).attr("fileHash");
    file.filedescriptioin = $(value).attr("filedescriptioin");

    return file;
}