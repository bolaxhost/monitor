﻿var currentUser; //Текущий пользователь
var isAddMeeting; //Права на добовления встречи

$(document).ready(function () {
    
    $.ajax({
        type: 'POST',
        url: '/CalendarEvents/GetPermitionFromMeeting',
        dataType: 'json',
        success: function (response) {
            isAddMeeting = response.IsAddMeeting;
        },
        async: false
    });
    
    $('body').on("click", ".menu-items-wrapper a", function (e) {
        var view = $('#calendarTasks').fullCalendar('getView');
        if(view.name=="month")
            return;
        if (e.currentTarget.id == "add_event_button") {
            event_start.val(clickDate);
            event_end.val(clickDate);
            formOpen('add');
        }
        if (e.currentTarget.id == "add_meeting_button") {
            window.location.href = '/Planning/CalendarEvents/AddMeeting?startDate=' + clickDate + '&returnUrl=' + window.location.pathname + '&viewName='+view.name;
        }
    });

    /* глобальные переменные */
    var event_start = $('#event_start');
    var event_end = $('#event_end');
    var event_desc = $('#event_desc');
    var event_error = $('#event_error');
    var calendar = $('#calendar');
    var form = $('#dialog-form');
    var event_id = $('#event_id');
    var event_entityId = $('#event_entityId');
    var event_type = $('#event_type');
    var event_createUser = $('#event_user');
    var event_urlToObject = $('#urlToObject');
    var format = "dd.MM.yyyy HH:mm";
    var clickDate;
    
    function emptyForm() {
        event_start.val("");
        event_end.val("");
        event_desc.val("");
        event_id.val("");
        event_entityId.val("");
        event_type.val("");
        event_error.text("");
        event_createUser.val("");
        event_desc.removeAttr("disabled");
        event_start.removeAttr("disabled");
        event_end.removeAttr("disabled");
        event_end.removeAttr("disabled");

        $("#referenceToOgject").hide();
        $("#workGroup").hide();
    }

    function formOpen(mode) {
        $('.menu-items-wrapper').hide();
        
        if (mode == 'add') {
            $('#add').show();
            $('#edit').hide();
            $("#delete").hide();
        }
        else if (mode == 'edit') {
            $('#edit').show();
            $('#add').hide();
            $("#delete").button("option", "disabled", false);
            $("#delete").show();
        }
        else if (mode == 'viewORdelete') {
            $('#edit').hide();
            $('#add').hide();
            $("#delete").button("option", "disabled", false);
            $("#delete").show();
            event_end.attr("disabled", "disabled");
            event_start.attr("disabled", "disabled");
            event_desc.attr("disabled", "disabled");
        }
        else if (mode == 'view') {
            $('#edit').hide();
            $('#add').hide();
            $("#delete").hide();
            event_end.attr("disabled", "disabled");
            event_start.attr("disabled", "disabled");
            event_desc.attr("disabled", "disabled");
        }

        if (event_urlToObject.attr("href") != undefined && event_urlToObject.attr("href") != "")
            event_urlToObject.parent().parent().show();

        form.dialog({ width: 600 });
        form.dialog('open');
        if (event_type.val() == 0)
            $(".ui-dialog-title").text("Личное событие");
        else if (event_type.val() == 1)
            $(".ui-dialog-title").text("Встреча");
        else if (event_type.val() == 2)
            $(".ui-dialog-title").text("Назначение на согласование");
            
        
    }

    var allowedDatesTask = new Object();

    $('#calendarTasks').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'prevYear,nextYear,month,agendaWeek,agendaDay'
        },
        viewDisplay: function (view) {
            var meetingMenu = $("<div class='menu-items-wrapper'><ul class='menu-items'>Создать событие:<li class='menu-item'><a href='#' id='add_event_button'>Личное событие</a></li><li class='menu-item'><a href='#' id='add_meeting_button'>Встреча</a></li></ul></div>");
            var eventMenu = $("<div class='menu-items-wrapper'><ul class='menu-items'>Создать событие:<li class='menu-item'><a href='#' id='add_event_button'>Личное событие</a></li></ul></div>");
            if (view.name == 'month')
                //доблвдение меню в ячейке  в зависимости от прав
            {
                if (isAddMeeting)
                    $(".fc-day").prepend(meetingMenu);
                else 
                    $(".fc-day").prepend(eventMenu);
                
            }

            if (view.name == 'agendaDay' || 'agendaWeek') {
               if (isAddMeeting)
                   $("#calendarTasks").prepend(meetingMenu);
                else
                   $("#calendarTasks").prepend(eventMenu);
            }
        },
        buttonText: {
            month: 'Месяц',
            week: 'Неделя',
            day: 'День',
            prev: "&nbsp;&#9668;&nbsp;",
            next: "&nbsp;&#9658;&nbsp;",
            prevYear: "&nbsp;&lt;&lt;&nbsp;",
            nextYear: "&nbsp;&gt;&gt;&nbsp;",
            today: "Сегодня"
        },
        //timeFormat: {
        //    agenda: 'hh:mm{ - hh:mm}',
        //    '': 'hh:mm'
        //},
        firstDay: 1,
        axisFormat: 'HH:mm',
        timeFormat: 'HH:mm',


        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
        dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],

        events: function (start, end, callback) {
            $.ajax({
                type: 'POST',
                url: '/Planning/CalendarEvents/InitCalendarEvents',
                dataType: 'json',
                data: { startDate: convertDates(start), endDate: convertDates(end) },
                success: function (response) {

                    allowedDatesTask['calendarEvents'] = response.allowedDates;

                    $('.fc-day-number').each(function () {

                        var date = convertDatesFormCell($(this).parent().parent().data('date'));

                        if (response.allowedDates != undefined) {
                            if ($.inArray(date, response.allowedDates) != -1) {
                                $(this).parent().parent().addClass("factoryHolliday");
                            }
                        }
                    });

                    if (!$('#calendarTasks').hasClass("daysOff")) {

                        var events = [];

                        $(response.events).each(function () {
                            $('#calendarTasks').fullCalendar('removeEvents').fullCalendar('removeEventSources');
                            events.push({
                                id: $(this).attr('id'),
                                type: $(this).attr('event_type'),
                                title: $(this).attr('event_desc'),
                                create_user: $(this).attr('modification_createdBy'),
                                event_entityId: $(this).attr('entity_id'),
                                allDay: false,
                                start: $.fullCalendar.formatDate(compareDate($(this).attr('EventDateTimeFromString')), 'yyyy-MM-dd HH:mm'),
                                end: $.fullCalendar.formatDate(compareDate($(this).attr('EventDateTimeToString')), 'yyyy-MM-dd HH:mm')
                            });
                        });
                        callback(events);
                    }
                },
                error: function (e) {
                }
            });
        }
        ,
        dayRender: function (date, cell) {
            if (allowedDatesTask['calendarEvents'] != undefined) {
                var check = convertDates(date);

                if ($.inArray(check, allowedDatesTask['calendarEvents']) != -1) {
                    cell.addClass("factoryHolliday");
                }
            }
        },
        dayClick: function (date, allDay, jsEvent, view) {
            if (!$('#calendarTasks').hasClass("daysOff")) {
                if ($(jsEvent.target).is('a#add_event_button')) {
                    var newDate = $.fullCalendar.formatDate(date, format);
                    event_start.val(newDate);
                    event_end.val(newDate);
                    formOpen('add');
                }
                else if ($(jsEvent.target).is('a#add_meeting_button')) {
                    var menuItemWrapper = $(this).find('.menu-items-wrapper');
                    menuItemWrapper.hide();
                    var newDate = $.fullCalendar.formatDate(date, format);
                    window.location.href = '/Planning/CalendarEvents/AddMeeting?startDate='+newDate+'&returnUrl='+ window.location.pathname;
                }
                else if (view.name == 'month') {
                    var menuItemWrappers = $('.menu-items-wrapper');
                    menuItemWrappers.hide();
                    var menuItemWrapper = $(this).find('.menu-items-wrapper');
                    menuItemWrapper.show();
                }
                
                else if (view.name == 'agendaDay' || 'agendaWeek') {
                    var menuItemWrappers = $('.menu-items-wrapper');
                    menuItemWrappers.hide();
                    var menuItemWrapper = $('#calendarTasks').find('.menu-items-wrapper');
                    menuItemWrapper.show();
                    menuItemWrapper.css("top", jsEvent.pageY).css("left", jsEvent.pageX - 100);
                    clickDate = $.fullCalendar.formatDate(date, format);
                }
            }
           
        },
        eventClick: function (calEvent, jsEvent, view) {
            event_id.val(calEvent.id);
            event_type.val(calEvent.type);
            event_desc.val(calEvent.title);
            event_start.val($.fullCalendar.formatDate(calEvent.start, format));
            event_end.val($.fullCalendar.formatDate(calEvent.end, format));
            event_createUser.val(calEvent.create_user);
            event_entityId.val(calEvent.event_entityId);

            if (event_createUser.val() == currentUser && event_type.val() == 0)
            { formOpen('edit'); }
            
            else if (event_createUser.val() == currentUser && event_type.val() != 0 && event_type.val() != 2)
            { formOpen('viewORdelete'); }
            else { formOpen('view'); }
            
        },
    });

    form.dialog({
        autoOpen: false,
        open: function (event, ui) {
            if ($(event_type).val() !=0) {
                $.post('/Planning/CalendarEvents/GetObjectReference', {id:$(event_id).val(), eventType: $(event_type).val(), objectId: $(event_entityId).val() }, function (data) {
                    if (data.ReferenceName != "" && data.ReferenceName != undefined) {
                        if ($(event_type).val() == 1) {
                            $("#dialog-form #event_workgroup").text(data.ReferenceName);
                            $("#workGroup").show();
                        }
                        if ($(event_type).val()==2) {
                        $("#dialog-form #reference").text(data.ReferenceName);
                        $("#dialog-form #reference").attr("href", data.Reference);
                        $("#referenceToOgject").show();
                        }
                        
                    }
                });
            }
        },
        buttons: [{
            id: 'add',
            text: 'Добавить',
            click: function () {
                $.ajax({
                    type: "POST",
                    url: '/Planning/CalendarEvents/EventOperation',
                    data: {
                        start: event_start.val(),
                        end: event_end.val(),
                        desc: event_desc.val(),
                        op: 'add'
                    },
                    success: function (response) {

                        if (response.error == undefined) {
                            $('#calendarTasks').fullCalendar('renderEvent', {
                                id: response.Id,
                                title: response.Description,
                                type: response.Type,
                                create_user: response.CreateUser,
                                //event_urlToObject:response.urlToObject,
                                allDay: false,
                                start: $.fullCalendar.formatDate(compareDate(response.StartDateString), 'yyyy-MM-dd HH:mm'),
                                end: $.fullCalendar.formatDate(compareDate(response.EndDateString), 'yyyy-MM-dd HH:mm')
                            }, true);
                            emptyForm();
                            form.dialog('close');
                        } else {
                            event_error.text(response.error);
                            formOpen('add');
                        }
                    },
                    error: function (e) {
                        event_error.text("Произошла ошибка сохранения!");
                        formOpen('add');
                    }
                });
            }
        },
{
    id: 'edit',
    text: 'Изменить',
    click: function () {
        $.ajax({
            type: "POST",
            url: '/Planning/CalendarEvents/EventOperation',
            data: {
                id: event_id.val(),
                start: event_start.val(),
                end: event_end.val(),
                desc: event_desc.val(),
                op: 'edit'
            },
            success: function (response) {
                if (response.error == undefined) {
                    $('#calendarTasks').fullCalendar('removeEvents', response.Id);

                    $('#calendarTasks').fullCalendar('renderEvent', {
                        id: response.Id,
                        title: response.Description,
                        type: response.Type,
                        create_user: response.CreateUser,
                        allDay: false,
                        start: $.fullCalendar.formatDate(compareDate(response.StartDateString), 'yyyy-MM-dd HH:mm'),
                        end: $.fullCalendar.formatDate(compareDate(response.EndDateString), 'yyyy-MM-dd HH:mm')
                    }, true);

                    emptyForm();
                    form.dialog('close');
                }
                else {
                    event_error.text(response.error);
                    formOpen('edit');
                }
            },
            error: function (e) {
                event_error.text("Произошла ошибка редактирования!");
                formOpen('edit');
            }
        });


    }
},
{
    id: 'cancel',
    text: 'Закрыть',
    click: function () {
        $(this).dialog('close');
        emptyForm();
    }
},
{
    id: 'delete',
    text: 'Удалить',
    click: function () {
       var result= confirm("Удалить выбранное событие?");
       if (result == true) {
            $.ajax({
                type: "POST",
                url: '/Planning/CalendarEvents/EventOperation',
                data: {
                    id: event_id.val(),
                    op: 'delete'
                },
                success: function(response) {
                    if (response.error == undefined) {
                        $('#calendarTasks').fullCalendar('removeEvents', response.Id);

                        emptyForm();
                        form.dialog('close');
                    } else {
                        event_error.text(response.error);
                        formOpen('delete');
                    }
                },
                error: function(e) {
                    event_error.text("Произошла ошибка удаления!");
                    formOpen('delete');
                }
            });
       } else {
           $(this).dialog('close');
           emptyForm();
       }
    },
    disabled: true
}]
    });


    function convertDates(date) {
        var d = date.getDate();
        var m = date.getMonth() + 1;
        var y = date.getFullYear();
        if (d < 10) {
            d = "0" + d;
        }
        if (m < 10) {
            m = "0" + m;
        }

        var dateStr = d + "." + m + "." + y;

        return dateStr;
    }

    function compareDate(stringDate) {
        //"dd.MM.yyyy HH:mm:ss"
        var d = parseInt(stringDate.substring(0, 2));
        var m = parseInt(stringDate.substring(3, 5));
        var y = parseInt(stringDate.substring(6, 10));
        var h = parseInt(stringDate.substring(11, 13));
        var mm = parseInt(stringDate.substring(14, 16));

        var date = new Date(y, m - 1, d, h, mm, 0, 0);
        return date;
    }


});

function convertDatesFormCell(stringDate) {
    //"2014-01-27""
    //debugger;

    var d = stringDate.substring(8, 10);
    var m = stringDate.substring(5, 7);
    var y = stringDate.substring(0, 4);

    var dateStr = d + "." + m + "." + y;

    return dateStr;
}