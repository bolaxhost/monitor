﻿using System.Web;
using System.Web.Optimization;

namespace NPS.WEB
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Single Page Application
            bundles.Add(new ScriptBundle("~/bundles/map-scripts").Include(
                        "~/Scripts/angular.min.js",
                        "~/Scripts/angular-route.min.js",
                        "~/Scripts/angular-resource.min.js",
                        "~/Scripts/angular-cookies.min.js",
                        "~/Scripts/angular-sanitize.min.js",
                        "~/Scripts/angular-ui/ui-bootstrap-tpls.min.js",

                        "~/Scripts/string.utilities.js",

                        "~/Scripts/biz/core/app-config.js",
                        "~/Scripts/biz/core/db-api.js",
                        "~/Scripts/biz/core/directives.js",
                        "~/Scripts/biz/core/controller-helpers.js"
                        ));
            bundles.Add(new StyleBundle("~/Content/map-css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/map.css"
                      ));
            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }
}
