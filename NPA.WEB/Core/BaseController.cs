﻿using System;
using System.Text; 
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
  
 
namespace NPS.WEB.Core
{
    public class BaseController : Controller
    {
        public JsonResult EmptySuccessResult()
        {
            return Json(new
            {
                state = "success"
            });
        }

        public JsonResult ErrorResult(Exception ex)
        {
            return ErrorResult(ex.Message);
        }

        public JsonResult ErrorResult(string message)
        {
            return Json(new
            {
                state = "error",
                message = message
            }, JsonRequestBehavior.AllowGet);
        }

        public string GetRequestContent()
        {
            var req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);

            return new System.IO.StreamReader(req).ReadToEnd();
        }

        public string ExceptionReport(Exception ex)
        {
            StringBuilder report = new StringBuilder();
            report.AppendLine(ex.Message);

            if (ex.InnerException != null)
                report.AppendLine(ExceptionReport(ex.InnerException));

            return report.ToString();  
        }
    }
}