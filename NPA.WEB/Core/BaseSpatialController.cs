﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
 
namespace NPS.WEB.Core
{
    public class BaseSpatialController : BaseController
    {
        public static string PostWFSData(string url, string data)
        {
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);

            req.Method = "POST";
            req.ContentType = "text/xml";

            byte[] sentData = Encoding.UTF8.GetBytes(data);
            req.ContentLength = sentData.Length;

            System.IO.Stream sendStream = req.GetRequestStream();
            sendStream.Write(sentData, 0, sentData.Length);
            sendStream.Close();

            System.Net.WebResponse res = req.GetResponse();

            System.IO.Stream ReceiveStream = res.GetResponseStream();
            System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8);

            //Кодировка указывается в зависимости от кодировки ответа сервера
            Char[] read = new Char[256];
            int count = sr.Read(read, 0, 256);
            string result = String.Empty;
            while (count > 0)
            {
                String str = new String(read, 0, count);
                result += str;
                count = sr.Read(read, 0, 256);
            }

            return result;
        }

        public static string GetWFSData(string url, string layer, string filter = "")
        {
            //http query
            var query = string.Format("{0}?service=WFS&version=1.1.0&request=GetFeature&typename={1}&outputFormat=application/json", url, layer);

            if (!string.IsNullOrWhiteSpace(filter))
                query = string.Format("{0}&{1}", query, filter);

            // Read the content.
            string responseFromServer = string.Empty;

            // Create a request for the URL. 
            WebRequest request = WebRequest.Create(query);

            // If required by the server, set the credentials.
            request.Credentials = CredentialCache.DefaultCredentials;
            // Get the response.
            WebResponse response = request.GetResponse();

            try
            {
                // Get the stream containing content returned by the server.
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);

                try
                {
                    // Read the content.
                    responseFromServer = reader.ReadToEnd();
                }
                finally
                {
                    // Clean up the streams and the response.
                    reader.Close();
                }
            }
            finally
            {
                response.Close();
            }

            return responseFromServer;
        }
    }
}