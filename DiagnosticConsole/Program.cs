﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading; 
using System.Threading.Tasks;
using System.IO;
using System.IO.Ports;

using System.Net;
using System.Net.Sockets;


/// <summary>
/// Консольное приложение для диагностирования оборудования
/// </summary>

namespace DiagnosticConsole
{
    class Program
    {
        private static TcpClient Connect(string address, int port)
        {
            Console.WriteLine("Соединение {0}:{1}...", address, port);
            return new TcpClient(address, port);
        }

        private static NetworkStream Send(TcpClient client, string message)
        {
            NetworkStream io = client.GetStream();

            Send(io, message);

            return io;
        }

        private static void Send(NetworkStream io, string message)
        {
            io.Write(System.Text.Encoding.ASCII.GetBytes(message), 0, message.Length);
            Thread.Sleep(10);
        }

        static void ConnectBySocket()
        {
            Console.Write("Введите IP (xx.xx.xx.xx): ");
            string id = Console.ReadLine();

            Console.Write("Введите номер порта: ");
            int port = Int32.Parse(Console.ReadLine());

            do
            {
                Console.Write("Введите команду: ");
                string command = Console.ReadLine() + "\r";

                using (TcpClient client = Connect(id, port))
                {
                    if (client.Connected)
                    {

                        NetworkStream io = Send(client, command);

                        while (!io.DataAvailable) { Thread.Sleep(10); }

                        byte[] data = new byte[1024];
                        int read = io.Read(data, 0, 1024);

                        string result = Encoding.ASCII.GetString(data, 0, read);
                        Console.WriteLine(string.Format("Ответ {0} байт \n{1}", read, result));
                    }
                }

                Console.WriteLine("E - выход, любая иная клавиша для продолжения");

            } while (Console.ReadKey(true).Key != ConsoleKey.E);
        }

        static void ConnectBySerialPort()
        {
            var ports = SerialPort.GetPortNames();
            foreach (var p in ports)
                Console.WriteLine(p);

            if (!ports.Any())
            {
                Console.WriteLine("Ни одного последовательного порта не обнаружено!");
                return;
            }

            Console.Write("Введите имя порта: ");
            string portName = Console.ReadLine();

            do
            {
                Console.Write("Введите команду: ");
                string command = Console.ReadLine() + "\r";

                var l = command.Length;

                //115200 8N1
                var sPort = new SerialPort(portName, 115200, Parity.None, 8, StopBits.One);
                sPort.Open();

                sPort.DataReceived += SerialPort_DataReceived;
                sPort.Write(command);


                Console.WriteLine("E - выход, любая иная клавиша для продолжения");

            } while (Console.ReadKey(true).Key != ConsoleKey.E);
        }

        static void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sPort = (SerialPort)sender;

            byte[] data = new byte[sPort.BytesToRead];
            int read = sPort.Read(data, 0, data.Length);

            string result = Encoding.ASCII.GetString(data, 0, read);
            Console.WriteLine(string.Format("Ответ {0} байт \n{1}", read, result));

            sPort.Close();
        }




        static StringBuilder _buffer = null;
        static int _bufferMaxSize = 256;
        static string _command = "";
        static SerialPort _sPort = null;

        delegate void CommandResultHandler(string result);
        static CommandResultHandler _resultHandler = null;

        static void ListenToSerialPort(string command = "", CommandResultHandler resultHandler = null)
        {
            var ports = SerialPort.GetPortNames();
            foreach (var p in ports)
                Console.WriteLine(p);

            if (!ports.Any())
            {
                Console.WriteLine("Ни одного последовательного порта не обнаружено!");
                return;
            }

            Console.Write("Введите имя порта: ");
            string portName = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(command))
            {
                Console.Write("Введите идентификатор предложения: ");
                _command = Console.ReadLine();
            }
            else
            {
                _command = command;
            }

            _resultHandler = resultHandler;

            if (_command.Length < 1)
            {
                Console.WriteLine("Непустимый идентификатор предложения");
                return;
            }

            _buffer = new StringBuilder();

            _sPort = new SerialPort(portName, 115200, Parity.None, 8, StopBits.One);
            _sPort.Open();
            _sPort.DataReceived += SerialPortListener_DataReceived;
        }

        static void SerialPortListener_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sPort = (SerialPort)sender;

            byte[] data = new byte[sPort.BytesToRead];
            int read = sPort.Read(data, 0, data.Length);

            string result = Encoding.ASCII.GetString(data, 0, read);

            _buffer.Append(result);

            if (_buffer.Length >= _bufferMaxSize)
            {
                var _str = _buffer.ToString();
                var _start = _str.IndexOf(_command);

                if (_start >= 0)
                {
                    var _end = _str.IndexOf("\r\n", _start);
                    if (_end >= 0)
                    {
                        var commandResult = _str.Substring(_start, _end - _start);

                        if (_resultHandler != null)
                        {
                            _resultHandler(commandResult);
                        }
                        else
                        {
                            Console.WriteLine("{0}: >> {1}", _command, commandResult);
                        }

                        _buffer = new StringBuilder(_str.Substring(_end + 2));
                    }
                }
            }
        }

        static List<double> _azm = new List<double>();
        static double _average = 0;
        static void HDTResultHandler(string result)
        {
            var parts = result.Split(new[] { ',' }, StringSplitOptions.None);

            if (parts.Length < 2)
            {
                return;
            }

            double azmValue = Utilities.ConvertToDouble(parts[parts.Length - 2]);

            _azm.Add(azmValue);
            int N = _azm.Count();

            if (N > 1)
            {
                _average = _average + (azmValue - _average) / N;

            } else
            {
                _average = azmValue;
            }

            Console.WriteLine("{0}: текущее значение: {1}; среднее значение: {2}", N, azmValue, _average);
        }





        static string ExceptionFullDescription(Exception ex)
        {
            if (ex.InnerException == null)
                return ex.Message;

            return string.Format("{0}\n{1}", ExceptionFullDescription(ex.InnerException), ex.Message);
        }

        static void TestSearching()
        {
            var sa = new Model.Searching(new Model.AnglesModel() { azimuth = 50, placeAngle = 3 });
            double prevSignal = 0;
            while (!sa.isCompleted)
            {
                Console.Write("Введите уровень сигнала: ");
                double signal = Utilities.ConvertToDouble(Console.ReadLine(), 0);
                if (signal == 0) signal = prevSignal;
                prevSignal = signal;
                sa.Do(signal, null);
            }

        }

        static void Main(string[] args)
        {
            Console.WriteLine("'S' - Связь через сокет (TCP/IP)");
            Console.WriteLine("'P' - Связь через последовательный порт (Serial Port)");
            Console.WriteLine("'L' - Слушать последовательный порт");
            Console.WriteLine("'G' - Слушать GPS компас");
            // Console.WriteLine("'T' - Тестирование алгоритма поиска и подстройки из положения (azm=50, el=3)");

            var key = Console.ReadKey(true).Key;

            try
            {
                if (key == ConsoleKey.S)
                {
                    ConnectBySocket();

                    Console.WriteLine("Работа завершена.");
                    Console.ReadKey(true);
                }
                else if (key == ConsoleKey.P)
                {
                    ConnectBySerialPort();

                    Console.WriteLine("Работа завершена.");
                    Console.ReadKey(true);
                }
                else if (key == ConsoleKey.L)
                {
                    ListenToSerialPort();
                    Console.WriteLine("Ожидание данных...");
                    Console.ReadKey(true);

                    _sPort.Close();
                    Console.WriteLine("Работа завершена.");
                    Console.ReadKey(true);
                }
                else if (key == ConsoleKey.G)
                {
                    ListenToSerialPort("HDT", HDTResultHandler);
                    Console.WriteLine("Ожидание данных...");
                    Console.ReadKey(true);

                    _sPort.Close();
                    Console.WriteLine("Работа завершена.");
                    Console.ReadKey(true);
                }
                else if (key == ConsoleKey.T)
                {
                    TestSearching();
                    Console.WriteLine("Работа завершена.");
                    Console.ReadKey(true);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ExceptionFullDescription(ex)); 
            }
        }
    }
}
