﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Threading;
using System.IO;
using CommonServices; 

using Newtonsoft.Json; 

namespace FlightService
{
    /// <summary>
    /// Режим работы службы
    /// </summary>
    public enum EControlMode
    {
        //Автосопровождение
        escort = 0,

        //Целеуказание
        targeting = 1,
    }

    public class FlightService : IFlightService
    {
        //Текущий режим работы сервиса
        private static EControlMode mode = EControlMode.escort;






        //Параметры базовой станции
        private static Object baseStationLockObject = new Object();
        private static Model.LocationModel _baseStation = null;
        private static Model.LocationModel BaseStation
        {
            get
            {
                lock (baseStationLockObject)
                {
                    return _baseStation; 
                }
            }

            set
            {
                lock (baseStationLockObject)
                {
                    _baseStation = value;
                }
            }
        }






        //Текущее состояние
        private static Object currentStateLockObject = new Object();
        private static Model.StateModel _currentState = null;
        private static Model.StateModel CurrentState
        {
            get
            {
                lock (currentStateLockObject)
                {
                    return _currentState;
                }
            }

            set
            {
                lock (currentStateLockObject)
                {
                    _currentState = value;
                }

            }
        }





        //Состояние алгоритма поиска
        private static Object searchingLockObject = new Object();
        private static Model.Searching _searchingState = null;
        private static Model.Searching Searching
        {
            get
            {
                lock (searchingLockObject)
                {
                    return _searchingState;
                }
            }

            set
            {
                lock (searchingLockObject)
                {
                    _searchingState = value;
                }

            }
        }




        //Сервис управления антеннами
        private static Object acmLockObject = new Object();
        private static IACMControl _acmControl = null;
        private static IACMControl AcmControl
        {
            get
            {
                lock (acmLockObject)
                {
                    return _acmControl;
                }
            }

            set
            {
                lock (acmLockObject)
                {
                    _acmControl = value;
                }

            }
        }





        //Строки отчета
        private static Object reportLockObject = new Object();
        private static List<string> _reportLines = null;
        private static List<string> ReportLines
        {
            get 
            {
                lock(reportLockObject) 
                {                    
                    return _reportLines ?? (_reportLines = new List<string>()); 
                }
            }
        }





        //Процесс формирования отчета (фоновая задача)
        private static CancellationTokenSource _cts;
        private static string _reportFileName;
        private static void PrintReportProcess()
        {
            //Файл отчета о полете
            _reportFileName = ConfigurationManager.AppSettings["reportFileName"];

            if (string.IsNullOrWhiteSpace(_reportFileName))
                return;

            _cts = new CancellationTokenSource();
            var ct = _cts.Token;

            while (true)
            {
                if (ct.IsCancellationRequested)
                {
                    return;
                }

                if (ReportLines.Count > 0)
                {
                    try
                    {
                        File.AppendAllLines(_reportFileName, ReportLines.ToArray(), Encoding.UTF8);
                        ReportLines.Clear();

                    }
                    catch (Exception ex) { Console.WriteLine($"ОШИБКА ПЕЧАТИ: {Utilities.ExceptionFullDescription(ex)}"); }
                }

                Task.Delay(100).Wait();
            }
        }






        //Инициализация
        static FlightService()
        {
            //Параметры базовой станции
            BaseStation = new Model.LocationModel()
            {
                time = DateTime.Now,
                lat = Utilities.ConvertToDouble(ConfigurationManager.AppSettings["baseStationLat"]),
                lon = Utilities.ConvertToDouble(ConfigurationManager.AppSettings["baseStationLon"]),
                pressure = Utilities.ConvertToDouble(ConfigurationManager.AppSettings["baseStationPressure"]),
                height = Utilities.ConvertToDouble(ConfigurationManager.AppSettings["baseStationHeight"])
            };

            //Управление антенной
            AcmControl = new ACMProxy();

            //Остановка задачи формирования отчета, если она была запущена
            _cts?.Cancel();

            //Запуск задачи формирования отчета
            Task.Run(() => PrintReportProcess());
        }








        ///-------------
        /// Private API
        ///-------------
        ///

        //Очистка состояния
        private void Clear()
        {
            if (FlightService.Searching != null)
            {
                FlightService.Searching.Break();
                FlightService.Searching = null; 
            }

            if (FlightService.CurrentState != null)
            {
                FlightService.CurrentState = null;
            }
        } 
        
        //Наведение антенны
        private void Guide(double azimuth, double placeAngle)
        {
            Task.Run(async () => { await AcmControl.SendAngles(azimuth, placeAngle); });
        }

        //Отмена наведения антенны
        private void Break()
        {
            Task.Run(async () => { await AcmControl.Break(); });
        }

        //Сохранение настроек базовой станции
        private void SaveBaseStationSettings()
        {
            Utilities.SetAppSettings("baseStationPressure", BaseStation.pressure.ToString());
            Utilities.SetAppSettings("baseStationLat", BaseStation.lat.ToString());
            Utilities.SetAppSettings("baseStationLon", BaseStation.lon.ToString());
            Utilities.SetAppSettings("baseStationHeight", BaseStation.height.ToString());
        }



        private static void OnSearchingSucceeded(object sender, Model.Searching.SearchingResults results)
        {

        }




        //-------------------------
        //Реализация IFlightService
        //-------------------------


        public void PostData(string type, string data)
        {
            switch (type)
            {
                case "signal" :
                    {
                        var signal = JsonConvert.DeserializeObject<Model.SignalModel>(data);
                        if (signal.isValid())
                        {
                            if (FlightService.Searching == null)
                            {
                                Console.WriteLine("FSRV: Получены данные ППМ: {0}", signal.ToString());
                            }

                            if (CurrentState != null)
                            {
                                CurrentState.Calculate(BaseStation);

                                //Целеуказанеие
                                if (mode == EControlMode.targeting && CurrentState.location != null && CurrentState.location.isValid())
                                {
                                    Guide(CurrentState.resultsByLocation.azimuth, CurrentState.resultsByLocation.placeAngle);
                                }

                                if (!string.IsNullOrWhiteSpace(_reportFileName))
                                {
                                    ReportLines.AddRange(CurrentState.MakeReport(!File.Exists(_reportFileName) && ReportLines.Count == 0));
                                }
                            }

                            //Создание нового состояния
                            CurrentState = new Model.StateModel(signal, CurrentState);

                            //Ограничение длины цепочки до 3-х (предотвращение утечки памяти и увеличения трафика)
                            if (CurrentState.prevState?.prevState?.prevState != null)
                            {
                                CurrentState.prevState.prevState.prevState = null;
                            }
                        }

                        //Отработка шага поиска
                        if (FlightService.Searching != null)
                        {
                            FlightService.Searching.Do(signal.dBm, AcmControl, CurrentState?.prevState?.angles);
                            if (FlightService.Searching.isCompleted)
                            {
                                Console.WriteLine("\n\n---===ПОИСК ЗАВЕРШЕН===---\n\n");
                                FlightService.Searching = null;
                            }
                        }

                        break;
                    }
                case "location":
                    {
                        var location = JsonConvert.DeserializeObject<Model.LocationModel>(data);
                        if (location.isValid() && CurrentState != null)
                        {
                            if (FlightService.Searching == null)
                            {
                                Console.WriteLine("FSRV: Получены данные БТ: {0}", location.ToString());
                            }

                            CurrentState.location = location;
                        }

                        break;
                    }
                case "angles":
                    {
                        var angles = JsonConvert.DeserializeObject<Model.AnglesModel>(data);
                        if (angles.isValid() && CurrentState != null)
                        {
                            if (FlightService.Searching == null)
                            {
                                Console.WriteLine("FSRV: Получены данные БУА: {0}", angles.ToString());
                            }

                            CurrentState.angles = angles;
                        }

                        break;
                    }
                case "azimuth-offset-set" :
                    {
                        double ao = Utilities.ConvertToDouble(data, 0);
                        (AcmControl as IACMTuning).AzimuthOffset = ao;
 
                        break;
                    }
            
                case "azimuth-offset-add" :
                    {
                        double ao = Utilities.ConvertToDouble(data, 0);
                        (AcmControl as IACMTuning).AzimuthOffset += ao;

                        break;
                    }
                case "place-angle-offset-set":
                    {
                        double pa = Utilities.ConvertToDouble(data, 0);
                        (AcmControl as IACMTuning).PlaceAngleOffset = pa;

                        break;
                    }

                case "place-angle-offset-add":
                    {
                        double pa = Utilities.ConvertToDouble(data, 0);
                        (AcmControl as IACMTuning).PlaceAngleOffset += pa;

                        break;
                    }

                case "base-station-pressure-set":
                    {
                        double p = Utilities.ConvertToDouble(data, 0);
                        BaseStation.pressure = p;
                        SaveBaseStationSettings();

                        break;
                    }
                case "settings" :
                    {
                        var settings = JsonConvert.DeserializeObject<Model.FlightSettings>(data);
                        if (settings.BaseStation != null)
                        {
                            BaseStation = settings.BaseStation;
                            SaveBaseStationSettings();
                        }

                        var acmt = (AcmControl as IACMTuning);

                        acmt.AzimuthOffset = settings.azimuthOffset;
                        acmt.PlaceAngleOffset = settings.placeAngleOffset;

                        break;
                    }
            }
        }

        public string GetData(string type)
        {
            switch (type)
            {
                case "base-station-parameters":
                    {
                        return JsonConvert.SerializeObject(BaseStation);  
                    }
                case "settings":
                    {
                        var acmt = (AcmControl as IACMTuning);

                        return JsonConvert.SerializeObject(new Model.FlightSettings() 
                        {
                            BaseStation = BaseStation,
                            azimuthOffset = acmt.AzimuthOffset,
                            placeAngleOffset = acmt.PlaceAngleOffset
                        });
                    }
            }

            return string.Empty; 
        }

        public void Execute(string type, string parameters = null)
        {
            switch (type)
            {
                case "search" :
                    {
                        var angles = JsonConvert.DeserializeObject<Model.AnglesModel>(parameters);
                        Console.WriteLine("\n\n---===Поступила команда ПОИСК из положения ({0},{1}).===---\n\n", angles.azimuth, angles.placeAngle);
                        FlightService.Searching = new Model.Searching(angles
                            , Utilities.ConvertToDouble(ConfigurationManager.AppSettings["SearchingStageOneRange"], 10)
                            , Utilities.ConvertToDouble(ConfigurationManager.AppSettings["SearchingStageOneStep"], 1)
                            , Utilities.ConvertToDouble(ConfigurationManager.AppSettings["SearchingStageTwoRange"], 2)
                            , Utilities.ConvertToDouble(ConfigurationManager.AppSettings["SearchingStageTwoStep"], 0.2)
                            );
                        FlightService.Searching.OnSuccess += OnSearchingSucceeded;

                        break;
                    }
                case "guide":
                    {
                        if (FlightService.Searching != null && !FlightService.Searching.isCompleted)
                        {
                            Console.WriteLine("\n\n---===Команда ручного наведения отменена! ПОИСК активен!===---\n\n");
                            return;
                        }

                        var angles = JsonConvert.DeserializeObject<Model.AnglesModel>(parameters);

                        Guide(angles.azimuth, angles.placeAngle);
                        break;
                    }
                case "break":
                    {
                        if (FlightService.Searching != null && !FlightService.Searching.isCompleted)
                        {
                            FlightService.Searching.Break();
                            FlightService.Searching = null;

                            Console.WriteLine("\n\n---===ПОИСК отменен!===---\n\n");
                            return;
                        }

                        Break();
                        break;
                    }
                case "clear" :
                    {
                        Clear(); 
                        break;
                    }
            }
        }

        public string GetState()
        {
            if (CurrentState == null)
                return string.Empty;

            if (CurrentState.prevState == null)
                return JsonConvert.SerializeObject(CurrentState);

            //Поскольку текущее состояния может быть еще не сформировано до конца, возвращается предыдущее состояние
            return JsonConvert.SerializeObject(CurrentState.prevState);
        }

        public void SetMode(ushort mode)
        {
            FlightService.mode = (EControlMode)mode;
            Console.WriteLine("\n\nРежим работы изменен: {0}\n\n", (FlightService.mode == EControlMode.targeting) ? "Целеуказание" : "Автосопровождение");
        }

        public ushort GetMode()
        {
            return (ushort)FlightService.mode;
        }
    }
}
