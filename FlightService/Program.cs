﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace FlightService
{
    class Program
    {
        /// <summary>
        /// Запуск службы в автономном режиме (не используется, поскольку служба запускается внутри FlightAdmin)
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Type serviceType = typeof(FlightService);
            using (System.ServiceModel.ServiceHost host = new System.ServiceModel.ServiceHost(serviceType))
            {
                host.Open();

                Console.WriteLine("Сервис сопровождения полета загружен.");
                Console.WriteLine("Нажмите <Q> для остановки сервиса.");

                while (Console.ReadKey().Key  != ConsoleKey.Q) {}

                host.Close();
            }
        }
    }
}
