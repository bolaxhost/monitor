﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel; 

/// <summary>
/// Интерфейс WCF службы FlightService
/// </summary>

namespace FlightService
{
    [ServiceContract]
    interface IFlightService
    {
        /// <summary>
        /// Отправка данных
        /// </summary>
        /// <param name="type">Тип данных</param>
        /// <param name="data">Значение (JSON)</param>
        [OperationContract]
        void PostData(string type, string data);


        /// <summary>
        /// Получение данных (JSON)
        /// </summary>
        /// <param name="type">Тип данных</param>
        /// <returns></returns>
        [OperationContract]
        string GetData(string type);



        /// <summary>
        /// Выполнение операции
        /// </summary>
        /// <param name="type">Тип операции</param>
        /// <param name="parameters">Параметры (JSON)</param>
        [OperationContract]
        void Execute(string type, string parameters = null);


        /// <summary>
        /// Получение текущего состояния
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        string GetState();



        /// <summary>
        /// Установка режима работы службы
        /// </summary>
        /// <param name="mode">автосопровождение = 0, целеуказани = 1</param>
        [OperationContract]
        void SetMode(ushort mode);



        /// <summary>
        /// Получение текущего режима работы служюы
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        ushort GetMode();
    }
}
