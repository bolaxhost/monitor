﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Threading; 

using System.Net;
using System.Net.Sockets;

namespace CommonServices
{
    /// <summary>
    /// Реализация управления антенной через БУА Сервер (Quick Deploy)
    /// </summary>
    public class ACMServiceBUA : IACMControl, IACMModel
    {
        private string _serverIp;
        private ushort _serverPort;

        internal ACMServiceBUA(string serverIp = null, ushort serverPort = 0)
        {
            if (string.IsNullOrWhiteSpace(serverIp))
                serverIp = ConfigurationManager.AppSettings["acmServerIp"];

            if (serverPort == 0)
                serverPort = Convert.ToUInt16(ConfigurationManager.AppSettings["acmServerPort"]);

            _serverIp = serverIp;
            _serverPort = serverPort; 
        }







        private string GetMessage(string body)
        {
            return string.Format("<?xml version=\"1.0 encoding= \"utf-8\"?>\n<oml version=\"2.0\">\n{0}\n</oml>", body);
        }

        private string GetHeader(string message)
        {
            return string.Format("{0}", message.Length).PadLeft(10, '0'); ;
        }

        private TcpClient Connect()
        {
            try
            {
                return new TcpClient(_serverIp, _serverPort);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ACMS: Ошибка:\n" + Utilities.ExceptionFullDescription(ex));
                return null;
            }
        }

        private NetworkStream GetStream(TcpClient client, string header, string message)
        {
            NetworkStream io = client.GetStream();
            io.Write(System.Text.Encoding.ASCII.GetBytes(header), 0, header.Length);
            Thread.Sleep(10);

            io.Write(System.Text.Encoding.ASCII.GetBytes(message), 0, message.Length);
            Thread.Sleep(10);

            return io;
        }







        //IACMService
        public bool IsValid
        {
            get { return !string.IsNullOrWhiteSpace(_serverIp) && _serverPort > 0; }
        }

        public async Task<Model.AnglesModel> Angles()
        {
            if (!this.IsValid)
                return null;

            Model.AnglesModel result = null;
            await Task.Run<Model.AnglesModel>(() =>
            {
                using (TcpClient client = Connect())
                {
                    if (client == null)
                        return result;

                    try
                    {
                        if (client.Connected)
                        {

                            var message = GetMessage("<TLM/>");
                            var header = GetHeader(message);

                            NetworkStream io = GetStream(client, header, message);

                            while (!io.DataAvailable) { Thread.Sleep(10); }

                            byte[] data = new byte[1024];
                            int read = io.Read(data, 0, 1024);

                            if (read == 10)
                            {
                                while (!io.DataAvailable) { Thread.Sleep(10); }

                                read = io.Read(data, 0, 1024);
                            }
                            else { return result; }

                            if (read == 34)
                            {
                                var azimuth = BitConverter.ToSingle(data, 17);
                                var placeAngle = BitConverter.ToSingle(data, 21);

                                result = new Model.AnglesModel()
                                {
                                    time = DateTime.Now,
                                    azimuth = azimuth,
                                    placeAngle = placeAngle
                                };
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("ACMS: Ошибка:\n" + Utilities.ExceptionFullDescription(ex));
                    }

                    return result;
                }

            }); 

            return result;
        }
        
        public async Task SendAngles(double azimuth, double placeAngle)
        {
            if (!this.IsValid)
                return;

            await Task.Run(() =>
            {
                using (TcpClient client = Connect())
                {
                    if (client == null)
                        return;

                    try
                    {
                        if (client.Connected)
                        {
                            var message = GetMessage(string.Format("<CUAS azm=\"{0}\" ugm=\"{1}\"></CUAS>", azimuth, placeAngle));
                            var header = GetHeader(message);

                            NetworkStream io = GetStream(client, header, message);

                            while (!io.DataAvailable) { Thread.Sleep(10); }

                            byte[] data = new byte[1024];
                            int read = io.Read(data, 0, 1024);
                        }

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("ACMS: Ошибка:\n" + Utilities.ExceptionFullDescription(ex));
                    }
                }
            }); 
        }
        
        public async Task Break()
        {
            if (!this.IsValid)
                return;

            await Task.Run(() =>
            {
                using (TcpClient client = Connect())
                {
                    if (client == null)
                        return;

                    try
                    {
                        if (client.Connected)
                        {

                            var message = GetMessage("<BREAK/>");
                            var header = GetHeader(message);

                            NetworkStream io = GetStream(client, header, message);

                            while (!io.DataAvailable) { Thread.Sleep(10); }

                            byte[] data = new byte[1024];
                            int read = io.Read(data, 0, 1024);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("ACMS: Ошибка:\n" + Utilities.ExceptionFullDescription(ex));
                    }
                }
            }); 
        }
    }
}
