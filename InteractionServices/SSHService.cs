﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;

using Renci.SshNet;
using Newtonsoft.Json;

namespace CommonServices
{
    /// <summary>
    /// Сервис получения уровня сигнала по протоколу SSH
    /// </summary>
    public class SSHService : IDisposable
    {
        private ConnectionInfo _connectionInfo = null;
        private SshClient _client = null;

        public SSHService(string prefix)
        {
            string ip = ConfigurationManager.AppSettings[prefix + "IP"];
            if (string.IsNullOrWhiteSpace(ip))
            {
                return;
            }

            int port = Utilities.ConvertToInt(ConfigurationManager.AppSettings[prefix + "Port"]);
            if (port == 0)
            {
                return;
            }

            string user = ConfigurationManager.AppSettings[prefix + "User"];
            string password = ConfigurationManager.AppSettings[prefix + "Password"];

            _connectionInfo = new ConnectionInfo(ip, port, user, new AuthenticationMethod[]
            {
                new PasswordAuthenticationMethod(user, password)
            });

        }
        public void Connect() 
        {
            if (_connectionInfo == null)
                return;

            if (_client != null)
                Disconnect();

            _client = new SshClient(_connectionInfo);
            _client.Connect();
        }
        public void Disconnect() 
        {
            if (_client == null)
                return;

            _client.Disconnect();
            _client = null;
        }

        public T ExecuteCommand<T>(string command)
        {
            if (_client == null)
                return JsonConvert.DeserializeObject<T>(string.Empty);

            using (var cmd = _client.CreateCommand(command))
            {
                return JsonConvert.DeserializeObject<T>(cmd.Execute());
            }
        }

        public void Dispose()
        {
            Disconnect();
        }






        public class TSignal
        {
            public string signal { get; set; }
        }

        public Model.SignalModel GetSignal()
        {
            TSignal[] data = ExecuteCommand<TSignal[]>("wstalist");

            if (data.Length == 0)
                return null;

            return new Model.SignalModel() { dBm = Utilities.ConvertToDouble(data[0].signal, 0), time = DateTime.Now };
        }
    }
}
