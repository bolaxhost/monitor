﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace CommonServices
{
    /// <summary>
    /// Proxy для унифицированного управления антеннами
    /// </summary>
    public class ACMProxy : IACMControl, IACMModel, IACMTuning
    {
        private double _azimuthOffset = 0;
        private double _placeAngleOffset = 0;

        private List<IACMControl> _acmControlList = null;
        private List<IACMModel> _acmModelList = null;

        public double AzimuthOffset
        {
            get
            {
                return _azimuthOffset; 
            }

            set
            {
                _azimuthOffset = value;
                Utilities.SetAppSettings("azimuthOffset", value.ToString());
            }
        }

        public double PlaceAngleOffset
        {
            get
            {
                return _placeAngleOffset;
            }

            set
            {
                _placeAngleOffset = value;
                Utilities.SetAppSettings("placeAngleOffset", value.ToString());
            }
        }

        public ACMProxy()
        {
            _azimuthOffset = Utilities.ConvertToDouble(ConfigurationManager.AppSettings["azimuthOffset"], 0);
            _placeAngleOffset = Utilities.ConvertToDouble(ConfigurationManager.AppSettings["placeAngleOffset"], 0);

            Console.WriteLine($"Заданы смещения по азимуту {_azimuthOffset} и углу места {_placeAngleOffset}");

            var acmAZV = new ACMServiceAZV();
            var acmBUA = new ACMServiceBUA();

            _acmControlList = new List<IACMControl>() { acmAZV, acmBUA };
            _acmModelList = new List<IACMModel>() { acmAZV, acmBUA };
        }


        public async Task<Model.AnglesModel> Angles()
        {
            foreach (var acm in _acmModelList)
                if (acm.IsValid)
                    return await acm.Angles();

            return null;
        }

        public async Task SendAngles(double azimuth, double placeAngle)
        {
            azimuth +=_azimuthOffset;
            placeAngle += _placeAngleOffset;

            foreach (var acm in _acmControlList)
                if (acm.IsValid)
                    await acm.SendAngles(azimuth, placeAngle);
        }

        public async Task Break()
        {
            foreach (var acm in _acmControlList)
                if (acm.IsValid)
                    await acm.Break();
        }

        public bool IsValid
        {
            get { return true; }
        }
    }
}
