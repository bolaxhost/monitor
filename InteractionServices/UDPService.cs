﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;
using Newtonsoft.Json;

using System.Net;
using System.Net.Sockets; 

namespace CommonServices
{
    /// <summary>
    /// Сервис получения телеметрии по протоколу UDP
    /// </summary>
    public class UDPService
    {
        string _ip = "";
        int _port = 0;
        public UDPService(string prefix)
        {
            _ip = ConfigurationManager.AppSettings[prefix + "IP"];            
            if (string.IsNullOrWhiteSpace(_ip))
            {
                return;
            }
            
            _port = Utilities.ConvertToInt(ConfigurationManager.AppSettings[prefix + "Port"]);
        }

        public string ExecuteCommand(string command)
        {
            if (string.IsNullOrWhiteSpace(_ip) || _port == 0)
            {
                return string.Empty;
            }

            Socket sending_socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            sending_socket.ReceiveTimeout = 800;  

            IPAddress send_to_address = IPAddress.Parse(_ip);
            IPEndPoint sending_end_point = new IPEndPoint(send_to_address, _port);

            byte[] send_buffer = Encoding.ASCII.GetBytes(command);
            sending_socket.SendTo(send_buffer, sending_end_point);

            byte[] response = new byte[1000];
            var length = sending_socket.Receive(response);

            return Encoding.ASCII.GetString(response, 0, length);
        }

        public Model.LocationModel GetLocation()
        {
            var items = ExecuteCommand("Location").Split(new char[] { ' ' });
            if (items.Length < 3)
                return null;

            return new Model.LocationModel() 
            {
                time = DateTime.Now,
                lat = Utilities.ConvertToDouble(items[0], 0),
                lon = Utilities.ConvertToDouble(items[1], 0),
                pressure = Utilities.ConvertToDouble(items[2], 0),
            };
        }
    }
}
