﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace CommonServices
{
    /// <summary>
    /// Реализация управления антенной через последовательный порт (старая антенна)
    /// </summary>
    public class ACMServiceAZV : IACMControl, IACMModel
    {
        //callback infrastructure
        internal delegate void OnResultReceived(object sender, string result);

        internal event OnResultReceived ResultReceivedEvent;

        protected virtual void RaiseResultReceived(string result)
        {
            var handler = ResultReceivedEvent;
            if (handler != null)
                handler(this, result);
        }
        
        internal bool _waitForResponce = false;




        private SerialPort _port = null;
        internal ACMServiceAZV(string serialPortName = null)
        {
            if (string.IsNullOrWhiteSpace(serialPortName))
            {
                serialPortName = ConfigurationManager.AppSettings["acm2SerialPortName"];
            }

            if (!string.IsNullOrWhiteSpace(serialPortName))
            {
                _port = new SerialPort(serialPortName, 115200, Parity.None, 8, StopBits.One);
                _port.DataReceived += SerialPort_DataReceived;
            }
        }








        internal bool SendCommand(string command)
        {
            if (_port == null)
                return false;

            if (!_port.IsOpen)
                _port.Open(); 

            _port.Write(command + "\r");

            return true;
        }
        private string DoubleToString(double value)
        {
            return string.Format("{0:0.00}", value).Replace(",", ".");
        }

        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (!_port.IsOpen)
                return;

            byte[] data = new byte[_port.ReadBufferSize];
            int read = _port.Read(data, 0, data.Length);

            _port.Close(); 

            RaiseResultReceived(Encoding.ASCII.GetString(data, 0, read));
        }






        //IACMService
        public bool IsValid
        {
            get { return _port != null; }
        }

        public async Task<Model.AnglesModel> Angles()
        {
            if (!this.IsValid)
                return null;

            if (_waitForResponce)
                return null;

            Model.AnglesModel angles = null;
            await Task.Run(() =>
            {
                try
                {
                    OnResultReceived h = (object sender, string result) =>
                    {
                        result = result.Trim();

                        var pos = result.IndexOf("OK", 0, StringComparison.InvariantCultureIgnoreCase);
                        if (pos != -1 && result.Length > 0)
                        {
                            result = result.Substring(pos + 2).Trim();

                            var values = result.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            if (values.Length >= 2)
                            {
                                try
                                {
                                    angles = new Model.AnglesModel()
                                    {
                                        time = DateTime.Now,
                                        azimuth = Utilities.ConvertToDouble(values[0]),
                                        placeAngle = Utilities.ConvertToDouble(values[1])
                                    };
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("ACMS: Ошибка:\n" + Utilities.ExceptionFullDescription(ex));
                                }
                            }
                        }

                        _waitForResponce = false;
                    };

                    this.ResultReceivedEvent += h;

                    _waitForResponce = true;

                    if (!SendCommand("y"))
                        _waitForResponce = false;

                    while (_waitForResponce)
                    {
                        Task.Delay(10).Wait(); 
                    }

                    this.ResultReceivedEvent -= h;

                }
                catch (Exception ex)
                {
                    Console.WriteLine("ACMS: Ошибка:\n" + Utilities.ExceptionFullDescription(ex));
                }
            });

            return angles;
        }

        public async Task SendAngles(double azimuth, double placeAngle)
        {
            if (!this.IsValid)
                return;
            
            await Task.Run(() =>
            {
                try
                {
                    SendCommand(string.Format("q{0} {1}", DoubleToString(azimuth), DoubleToString(placeAngle)));
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ACMS: Ошибка:\n" + Utilities.ExceptionFullDescription(ex));
                }
            }); 
        }

        public async Task Break()
        {
            if (!this.IsValid)
                return;

            await Task.Run(() =>
            {
                try
                {
                    SendCommand("s");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ACMS: Ошибка:\n" + Utilities.ExceptionFullDescription(ex));
                }
            });
        }
    }
}
