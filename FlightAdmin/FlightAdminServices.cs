﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using System.ServiceModel;

using Newtonsoft.Json;

using CommonServices;
using FlightAdmin.FlightServiceReference; 

namespace FlightAdmin
{
    /// <summary>
    /// Определение асинхронных задач получения данных и запуска FlightService (WCF служба)
    /// </summary>

    public static class FlightAdminService
    {
        //Токен отмены выполнения асинхронных задач
        private static CancellationTokenSource _cts = null;
        //Финальная задача, завершающаяся после окончания всех запущенных асинхронно задач
        private static Task _finalTask = null;

        //Текущее состояние FlightService
        private static Model.StateModel _state = null;
        private static object _stateLock = new Object();
        public static Model.StateModel getState()
        {
            lock (_stateLock)
            {
                return _state;
            }
        }
        public static void setState(Model.StateModel state)
        {
            lock (_stateLock)
            {
                _state = state;
            }
        }




        //Уровни сигнала от ближней и дальней антенн
        private static double? _dBmN, _dBm;
        public static double dBm { get { return _dBm.HasValue ? _dBm.Value : 0;  } }
        public static double dBmN { get { return _dBmN.HasValue ? _dBmN.Value : 0; } }






        /// <summary>
        /// Запуск всех задач получения данных
        /// </summary>
        /// <param name="simulation">Запуск симулияции, если true</param>
        /// <param name="acm">Запуск задачи получения углов от антенны, если true (Может работать некорректно, поэтому рекомендуется не использовать)</param>
        public static void StartAll(bool simulation, bool acm)
        {
            //Остановка всех задач (если они были запущены)
            StopAll();

            //Инициализация списка задач
            List<TaskAction> tasks = new List<TaskAction>();

            //Добавление задачи: Старт FlightService
            tasks.Add(StartFlightService);

            //Добавление задачи: Мониторинг состояния FlightService
            tasks.Add(MonitorFlightService);

            if (simulation)
            {
                //Добавление задач симуляции
                tasks.Add((ct) => SimulationTask(ct, "signal"));
                tasks.Add((ct) => SimulationTask(ct, "location"));
                if (acm) tasks.Add((ct) => SimulationTask(ct, "angles"));
            }
            else
            {
                //Добавление задачи: Получение уровня сигнала
                tasks.Add((ct) => MonitorSSH(ct, "signal"));

                //Добавление задачи: Получение телеметрии
                tasks.Add((ct) => MonitorUDP(ct, "location"));

                //Добавление задачи: Получение телеметрии для окружения (важно давление в месте работы антенны)
                tasks.Add((ct) => MonitorENV(ct, "environment"));

                //Добавление задачи: Получение углов от антенны
                if (acm) tasks.Add((ct) => MonitorACM(ct, "angles"));
            }

            //Запуск всех задач
            Task.Run(() => { _finalTask = Task.WhenAll(tasks.Select(i => { var t = StartTask(i); Task.Delay(100).Wait(); return t; })); }); 
        }

        /// <summary>
        /// Остановка всех задач
        /// </summary>
        public static void StopAll()
        {
            if (_cts == null)
                return;

            _cts.Cancel();
            _cts = null;

            //Ожидание завершения всех задач
            _finalTask.Wait();
            _finalTask = null;
        }

        /// <summary>
        /// Сигнатура задачи
        /// </summary>
        /// <param name="ct"></param>
        public delegate void TaskAction(CancellationToken ct);
        /// <summary>
        /// Запуск асинхронной задачи
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public static Task StartTask(TaskAction action)
        {
            _cts = _cts ?? new CancellationTokenSource();

            return Task.Run(() =>
            {
                try
                {
                    action(_cts.Token);
                }
                catch (AggregateException ex)
                {
                    Console.WriteLine(Utilities.ExceptionFullDescription(ex));

                    foreach (var ie in ex.InnerExceptions)
                        Console.WriteLine(Utilities.ExceptionFullDescription(ie));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(Utilities.ExceptionFullDescription(ex));
                }
            });
        }















        /// <summary>
        /// Пауза
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        public static void Pause(DateTime start, DateTime end)
        {
            Pause(1000 - TimeSpanToMilliseconds(end - start));
        }
        private static void Pause(TimeSpan span)
        {
            Pause(TimeSpanToMilliseconds(span));
        }
        private static void Pause(int milliseconds)
        {
            if (milliseconds <= 0)
                return;

            Task.Delay(milliseconds).Wait();  
        }
        private static int TimeSpanToMilliseconds(TimeSpan span)
        {
            return span.Seconds * 1000 + span.Milliseconds;
        }










        /// <summary>
        /// Запуск WCF службы FlightService
        /// </summary>
        /// <param name="ct"></param>
        static void StartFlightService(CancellationToken ct)
        {
            Type serviceType = typeof(FlightService.FlightService);
            using (System.ServiceModel.ServiceHost host = new System.ServiceModel.ServiceHost(serviceType))
            {
                try
                {
                    host.Open();

                    Console.WriteLine("FlightService запущен.");

                    //Очистка всех статических данных службы
                    using (var fs = new FlightServiceClient())
                    {
                        fs.Execute("clear", string.Empty);
                    }

                    while (!ct.IsCancellationRequested)
                    {
                        Task.Delay(100).Wait();
                    }

                    host.Close();

                }
                catch (TimeoutException timeProblem)
                {
                    Console.WriteLine(Utilities.ExceptionFullDescription(timeProblem));
                }
                catch (CommunicationException commProblem)
                {
                    Console.WriteLine(Utilities.ExceptionFullDescription(commProblem));
                }
                finally
                {
                    Console.WriteLine("FlightService остановлен.");
                }
            }
        }









        /// <summary>
        /// Задача мониторинга состояния FlightService
        /// </summary>
        /// <param name="ct"></param>
        /// <param name="parameters"></param>
        static void MonitorFlightService(CancellationToken ct)
        {
            using (var fs = new FlightServiceClient())
            {
                while (true)
                {
                    if (ct.IsCancellationRequested)
                    {
                        return;
                    }

                    //Сканирование
                    DateTime start = DateTime.Now;
                    try
                    {
                        FlightAdminService.setState(Newtonsoft.Json.JsonConvert.DeserializeObject<Model.StateModel>(fs.GetState()));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("INFO: Ошибка:\n{0}", Utilities.ExceptionFullDescription(ex));
                    }

                    if (ct.IsCancellationRequested)
                    {
                        return;
                    }
                    DateTime end = DateTime.Now;

                    //Пауза
                    Pause(start, end);
                }
            }
        }











        /// <summary>
        /// Задача мониторинга состояния антенны
        /// </summary>
        /// <param name="ct"></param>
        /// <param name="parameters"></param>
        static void MonitorACM(CancellationToken ct, string parameters)
        {
            IACMModel acmService = new ACMProxy();

            using (var fs = new FlightServiceClient())
            {
                while (true)
                {
                    if (ct.IsCancellationRequested)
                    {
                        Console.WriteLine("ACMS: Задача сканирования отменена!");
                        return;
                    }


                    //Сканирование
                    DateTime start = DateTime.Now;
                    try
                    {
                        ScanACM(fs, acmService);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("ACMS: Ошибка:\n{0}", Utilities.ExceptionFullDescription(ex));
                    }


                    if (ct.IsCancellationRequested)
                    {
                        Console.WriteLine("ACMS: Задача сканирования отменена!");
                        return;
                    }
                    DateTime end = DateTime.Now;

                    //Пауза
                    Pause(start, end);
                }
            }
        }

        static void ScanACM(FlightServiceClient fs, IACMModel acmService)
        {
            Model.AnglesModel result = acmService.Angles().Result;

            if (result == null)
                result = acmService.Angles().Result;

            if (result != null)
            {
                Console.WriteLine("ACMS: Отправлены данные: {0}", result.ToString());
                fs.PostData("angles", JsonConvert.SerializeObject(result));
            }
        }













        /// <summary>
        /// Задача мониторинга телематического блока
        /// </summary>
        /// <param name="ct"></param>
        /// <param name="parameters"></param>
        private static void MonitorUDP(CancellationToken ct, string parameters)
        {
            UDPService udpService = new UDPService("UDP_");

            using (var fs = new FlightServiceClient())
            {
                while (true)
                {
                    if (ct.IsCancellationRequested)
                    {
                        Console.WriteLine("UDPS: Задача сканирования UDP отменена!");
                        return;
                    }

                    //Сканирование
                    DateTime start = DateTime.Now;
                    try
                    {
                        ScanUDP(fs, udpService);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("UDPS: Ошибка получения данных.");
                        Console.WriteLine(ex);
                    }

                    if (ct.IsCancellationRequested)
                    {
                        Console.WriteLine("UDPS: Задача сканирования UDP отменена!");
                        return;
                    }
                    DateTime end = DateTime.Now;

                    //Пауза
                    Pause(start, end);
                }
            }
        }

        private static void ScanUDP(FlightServiceClient fs, UDPService udpService)
        {
            var result = udpService.GetLocation();
            if (result != null)
            {
                Console.WriteLine("UDPS: Отправлены данные: {0}", result.ToString());
                fs.PostData("location", JsonConvert.SerializeObject(result));
            }
            else
            {
                fs.PostData("location", JsonConvert.SerializeObject(new Model.LocationModel()));
            }
        }





        /// <summary>
        /// Задача мониторинга окружения (второго телематического блока, установленного рядом с антенной)
        /// </summary>
        /// <param name="ct"></param>
        /// <param name="parameters"></param>
        private static void MonitorENV(CancellationToken ct, string parameters)
        {
            UDPService udpService = new UDPService("ENV_");

            using (var fs = new FlightServiceClient())
            {
                while (true)
                {
                    if (ct.IsCancellationRequested)
                    {
                        Console.WriteLine("ENVS: Задача сканирования ENV отменена!");
                        return;
                    }

                    //Сканирование
                    DateTime start = DateTime.Now;
                    try
                    {
                        ScanENV(fs, udpService);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("ENVS: Ошибка получения данных.");
                        Console.WriteLine(ex);
                    }

                    if (ct.IsCancellationRequested)
                    {
                        Console.WriteLine("ENVS: Задача сканирования ENV отменена!");
                        return;
                    }
                    DateTime end = DateTime.Now;

                    //Пауза
                    Pause(start, end);
                }
            }
        }
        
        private static void ScanENV(FlightServiceClient fs, UDPService udpService)
        {
            var result = udpService.GetLocation();
            if (result != null)
            {
                Console.WriteLine("ENVS: Давление в месте слежения: {0}", result.pressure);
            }
        }












        /// <summary>
        /// Задача мониторинга уровня сигнала от двух антенн: SSH - дальней и SSHN - ближней
        /// </summary>
        /// <param name="ct"></param>
        /// <param name="parameters"></param>
        private static void MonitorSSH(CancellationToken ct, string parameters)
        {
            SSHService sshService = new SSHService("SSH_");

            try
            {
                sshService.Connect();
            }
            catch (Exception ex)
            {
                Console.WriteLine("SSHS: Ошибка соединения.");
                Console.WriteLine(ex);
                return;
            }

            //Ближняя зона
            SSHService sshServiceN = new SSHService("SSHN_");

            try
            {
                sshServiceN.Connect();
            }
            catch (Exception ex)
            {
                Console.WriteLine("SSHS: Ошибка соединения (ближняя зона).");
                Console.WriteLine(ex);
                sshServiceN = null;
            }

            using (var fs = new FlightServiceClient())
            {
                while (true)
                {
                    if (ct.IsCancellationRequested)
                    {
                        Console.WriteLine("SSHS: Задача сканирования SSH отменена!");
                        return;
                    }

                    //Сканирование
                    DateTime start = DateTime.Now;
                    try
                    {
                        ScanSSH(fs, sshService, sshServiceN);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("SSHS: Ошибка получения данных.");
                        Console.WriteLine(ex);
                    }

                    if (ct.IsCancellationRequested)
                    {
                        Console.WriteLine("SSHS: Задача сканирования SSH отменена!");
                        return;
                    }
                    DateTime end = DateTime.Now;

                    //Пауза
                    Pause(start, end);
                }
            }
        }

        private static void ScanSSH(FlightServiceClient fs, SSHService sshService, SSHService sshServiceN)
        {
            Model.SignalModel result = sshService.GetSignal();
            _dBm = result?.dBm;  

            if (sshServiceN != null)
            {
                Model.SignalModel resultN = sshServiceN.GetSignal();
                _dBmN = resultN?.dBm; 

                Console.WriteLine("SSHS: Ближняя зона {0} dBm; Дальняя зона {1} dBm.", (resultN != null && resultN.isValid()) ? resultN.dBm.ToString() : "---", (result != null && result.isValid()) ? result.dBm.ToString() : "---");

                if (resultN != null && resultN.isValid())
                {
                    if (result == null || !result.isValid() || result.dBm < resultN.dBm)
                    {
                        result = resultN;
                    }
                }
            }

            if (result != null)
            {
                Console.WriteLine("SSHS: Отправлены данные ППМ: {0}", result.ToString());
                fs.PostData("signal", JsonConvert.SerializeObject(result));
            }
            else
            {
                fs.PostData("signal", JsonConvert.SerializeObject(new Model.SignalModel()));
            }
        }






















        /// <summary>
        /// Задачи симуляции
        /// </summary>
        /// <param name="ct"></param>
        /// <param name="sourceType"></param>
        private static void SimulationTask(CancellationToken ct, string sourceType)
        {
            string filename = ConfigurationManager.AppSettings["simulation_" + sourceType];
            using (var fs = new FlightServiceClient())
            {
                string[] lines = System.IO.File.ReadAllLines(filename);

                Console.WriteLine(string.Format("SIM_{0}: Прочитано {1} состояний.", sourceType, lines.Count()));

                DateTime prevTime = DateTime.MinValue;
                foreach (var line in lines)
                {
                    if (ct.IsCancellationRequested)
                    {
                        Console.WriteLine("SIM_{0}: Задача симуляции отменена!", sourceType);
                        return;
                    }

                    //Имитация сканирования
                    DateTime start = DateTime.Now;

                    DateTime time = DateTime.MinValue;
                    HandleValues(fs, sourceType, GetStringValues(line), ref time);

                    if (ct.IsCancellationRequested)
                    {
                        Console.WriteLine("SIM_{0}: Задача симуляции отменена!", sourceType);
                        return;
                    }

                    DateTime end = DateTime.Now;

                    if (prevTime == DateTime.MinValue)
                    {
                        Pause(start, end);
                    }
                    else if (time != prevTime)
                    {
                        Pause(time - prevTime - (end - start));
                    }

                    prevTime = time;
                }

                Console.WriteLine("SIM_{0}: симуляция завершена.", sourceType);
            }

        }
        static string[] GetStringValues(string line)
        {
            return line.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
        }


        static void HandleValues(FlightServiceClient fs, string sourceType, string[] values, ref DateTime time)
        {
            string data = string.Empty;
            switch (sourceType)
            {
                case "location":
                    {
                        if (values.Length == 4)
                        {
                            time = Utilities.ConvertToDateTime(values[0]);

                            double lat = Utilities.ConvertToDouble(values[1]);
                            double lon = Utilities.ConvertToDouble(values[2]);
                            double pressure = Utilities.ConvertToDouble(values[3]);

                            if (lat > 0 && lon > 0)
                            {
                                data = JsonConvert.SerializeObject(new Model.LocationModel()
                                {
                                    time = time,
                                    lat = lat,
                                    lon = lon,
                                    pressure = pressure,
                                });
                            }
                        }

                        break;
                    }
                case "signal":
                    {
                        if (values.Length == 2)
                        {
                            time = Utilities.ConvertToDateTime(values[0]);

                            double dBm = Utilities.ConvertToDouble(values[1]);

                            if (dBm != 0)
                            {
                                data = JsonConvert.SerializeObject(new Model.SignalModel()
                                {
                                    time = time,
                                    dBm = dBm,
                                });
                            }


                            _dBm = _dBmN = dBm;
                        }

                        break;
                    }
                case "angles":
                    {
                        if (values.Length == 3)
                        {
                            time = Utilities.ConvertToDateTime(values[0]);

                            double azimuth = Utilities.ConvertToDouble(values[1]);
                            double placeAngle = Utilities.ConvertToDouble(values[2]);

                            if (placeAngle != 0)
                            {
                                data = JsonConvert.SerializeObject(new Model.AnglesModel()
                                {
                                    time = time,
                                    azimuth = azimuth,
                                    placeAngle = placeAngle
                                });
                            }

                        }

                        break;
                    }
            }

            if (string.IsNullOrWhiteSpace(data))
                return;

            fs.PostData(sourceType, data);
        }
    }
}
