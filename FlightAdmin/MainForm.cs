﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FlightAdmin
{
    /// <summary>
    /// Главная форма приложения
    /// </summary>
    public partial class MainForm : Form
    {
        TextWriter _writer = null;

        public MainForm()
        {
            InitializeComponent();

            //Инициализация сервиса обработки потока вывода консоли
            _writer = new TextBoxStreamWriter(textBoxConsole, textBoxErrorConsole);

            //Инициализация консоли
            Console.SetOut(_writer);
        }

        /// <summary>
        /// Обработка "Старт": Запуск всех задач в асинхронном режиме в соответствии с текущими настройками
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Enabled = false;

            textBoxConsole.Text = "";
            textBoxErrorConsole.Text = "";

            UpdateStateProperties(null);

            //Старт задач получения данных
            FlightAdminService.StartAll(checkBoxSimulation.CheckState == CheckState.Checked, checkBoxACM.CheckState == CheckState.Checked);

            //Старт мониторинга за службой FlightService
            FlightAdminService.StartTask((ct) => MonitorFlightService(ct));

            btnStop.Enabled = true; 
        }


        /// <summary>
        /// Обработка "Стоп": Остановка всех задач
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStop_Click(object sender, EventArgs e)
        {
            btnStop.Enabled = false;
            FlightAdminService.StopAll();
            btnStart.Enabled = true;
        }


        /// <summary>
        /// Получение текущего состояния FlightService (WCF служба)
        /// </summary>
        /// <param name="ct"></param>
        /// <param name="parameters"></param>
        void MonitorFlightService(CancellationToken ct)
        {
            while (true)
            {
                if (ct.IsCancellationRequested)
                {
                    return;
                }

                //Сканирование
                DateTime start = DateTime.Now;
                try
                {
                    UpdateStateProperties(FlightAdminService.getState());
                }
                catch (Exception ex)
                {
                    Console.WriteLine("INFO: Ошибка:\n{0}", Utilities.ExceptionFullDescription(ex));
                }
                DateTime end = DateTime.Now;


                if (ct.IsCancellationRequested)
                {
                    return;
                }

                //Пауза
                FlightAdminService.Pause(start, end);
            }
        }

        /// <summary>
        /// Обновление элементов управления в соответствии с текущим состоянием FlightService
        /// </summary>
        /// <param name="state"></param>
        private void UpdateStateProperties(Model.StateModel state)
        {
            if (state == null)
            {
                SetText(textStateDate, "");
                SetText(textStateTime, "");

                SetText(textStateLat, "");
                SetText(textStateLon, "");
                SetText(textStatePressure, "");

                SetText(textStateSignal, "");

                SetText(text_dBm, "");
                SetText(text_dBmN, "");

                SetSignalIndicators(textSignalIndicator0, textSignalIndicatorA, textSignalIndicatorB, textSignalIndicatorC, textSignalIndicatorD, 0);
                SetSignalIndicators(textSignalIndicatorN0, textSignalIndicatorNA, textSignalIndicatorNB, textSignalIndicatorNC, textSignalIndicatorND, 0);

                return;
            }

            SetText(textStateDate, DateTime.Now.ToShortDateString());
            SetText(textStateTime, state.time.ToString("HH:mm:ss"));

            if (state.location != null)
            {
                SetText(textStateLat, state.location.lat.ToString("#.00"), state.location.isApproximated);
                SetText(textStateLon, state.location.lon.ToString("#.00"), state.location.isApproximated);
                SetText(textStatePressure, state.location.pressure.ToString("#.00"), state.location.isApproximated);
            }
            else
            {
                SetText(textStateLat, "--", true);
                SetText(textStateLon, "--", true);
                SetText(textStatePressure, "--", true);
            }

            if (state.signal != null)
            {
                var signal = state.signal.dBm;
                if (signal > -83)
                {
                    SetText(textStateSignal, signal.ToString());
                }
                else
                {
                    SetText(textStateSignal, signal.ToString(), true);
                }

                SetText(text_dBm, FlightAdminService.dBm.ToString("#.00"));
                SetText(text_dBmN, FlightAdminService.dBmN.ToString("#.00"));

                SetSignalIndicators(textSignalIndicator0, textSignalIndicatorA, textSignalIndicatorB, textSignalIndicatorC, textSignalIndicatorD, FlightAdminService.dBm);
                SetSignalIndicators(textSignalIndicatorN0, textSignalIndicatorNA, textSignalIndicatorNB, textSignalIndicatorNC, textSignalIndicatorND, FlightAdminService.dBmN);
            }
            else
            {
                SetSignalIndicators(textSignalIndicator0, textSignalIndicatorA, textSignalIndicatorB, textSignalIndicatorC, textSignalIndicatorD, 0);
                SetSignalIndicators(textSignalIndicatorN0, textSignalIndicatorNA, textSignalIndicatorNB, textSignalIndicatorNC, textSignalIndicatorND, 0);
            }

        }

        /// <summary>
        /// Потокобезопасная установка текста в TextBox
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="value"></param>
        /// <param name="warning"></param>
        static void SetText(TextBox textBox, string value, bool warning = false)
        {
            Action AppendText = () =>
            {
                textBox.Text = value;

                if (warning) {
                    textBox.ForeColor = Color.White;
                    textBox.BackColor = Color.Red;
                }
                else
                {
                    textBox.ForeColor = Color.Black;
                    textBox.BackColor = Color.White;
                }
            };

            if (textBox.InvokeRequired)
            {
                textBox.BeginInvoke((Action)AppendText);
            }
            else
            {
                AppendText();
            }
        }
        
        /// <summary>
        /// Установка индикаторов по уровню сигнала
        /// </summary>
        /// <param name="t0"></param>
        /// <param name="tA"></param>
        /// <param name="tB"></param>
        /// <param name="tC"></param>
        /// <param name="tD"></param>
        /// <param name="signal"></param>
        private void SetSignalIndicators(TextBox t0, TextBox tA, TextBox tB, TextBox tC, TextBox tD, double signal)
        {
            if (signal == 0)
            {
                SetSignalIndicator(t0, false);
                SetSignalIndicator(tA, false);
                SetSignalIndicator(tB, false);
                SetSignalIndicator(tC, false);
                SetSignalIndicator(tD, false);

                return;
            }

            SetSignalIndicator(t0, true);
            SetSignalIndicator(tA, signal >= -90);
            SetSignalIndicator(tB, signal >= -80);
            SetSignalIndicator(tC, signal >= -73);
            SetSignalIndicator(tD, signal >= -65);
        }

        /// <summary>
        /// Установка индикатора по уровню сигнала
        /// </summary>
        /// <param name="indicator"></param>
        /// <param name="visible"></param>
        static void SetSignalIndicator(TextBox indicator, bool visible)
        {
            Action UpdateIndicator = () =>
            {
                indicator.Visible = visible; 
            };

            if (indicator.InvokeRequired)
            {
                indicator.BeginInvoke((Action)UpdateIndicator);
            }
            else
            {
                UpdateIndicator();
            }
        }
    }


    /// <summary>
    /// Служебный класс - сервис, переопределяющий обработку потока вывода консоли. Вывод осуществляется на элементы управления MainForm.
    /// </summary>
    public class TextBoxStreamWriter : TextWriter
    {
        TextBox _output = null;
        TextBox _errors = null;

        public TextBoxStreamWriter(TextBox output, TextBox errors)
        {
            _output = output;
            _errors = errors;
        }

        public override void WriteLine(string value)
        {
            base.WriteLine(value);
            AppendText(value + "\n");
        }

        public override void Write(string value)
        {
            base.Write(value);
            AppendText(value);
        }

        public override void WriteLine(object value)
        {
            var ex = value as Exception;
            if (ex != null)
            {
                AppendError(Utilities.ExceptionFullDescription(ex) + "\n");
            }
            else
            {
                base.WriteLine(value);
            }
        }

        public override Encoding Encoding
        {
            get { return System.Text.Encoding.UTF8; }
        }

        private void AppendText(string value)
        {

            Action AppendText = () => 
            {
                if (_output.Text.Length > 30000)
                    _output.Text = "";
 
                _output.AppendText(string.Format("{0}: {1}", DateTime.Now.ToString("HH:mm:ss"), value));
            };

            if (_output.InvokeRequired)
            {
                _output.BeginInvoke((Action)AppendText);
            }
            else
            {
                AppendText();
            }
        }
        private void AppendError(string value)
        {

            Action AppendError = () =>
            {
                if (_errors.Text.Length > 30000)
                    _errors.Text = "";

                _errors.AppendText(string.Format("{0}: {1}", DateTime.Now.ToString("HH:mm:ss"), value));
            };

            if (_errors.InvokeRequired)
            {
                _errors.BeginInvoke((Action)AppendError);
            }
            else
            {
                AppendError();
            }
        }
    }
}
