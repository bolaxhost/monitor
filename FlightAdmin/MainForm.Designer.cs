﻿namespace FlightAdmin
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxConsole = new System.Windows.Forms.TextBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.checkBoxSimulation = new System.Windows.Forms.CheckBox();
            this.checkBoxACM = new System.Windows.Forms.CheckBox();
            this.textStateDate = new System.Windows.Forms.TextBox();
            this.textStateTime = new System.Windows.Forms.TextBox();
            this.textStateLat = new System.Windows.Forms.TextBox();
            this.textStateLon = new System.Windows.Forms.TextBox();
            this.textStatePressure = new System.Windows.Forms.TextBox();
            this.text_dBm = new System.Windows.Forms.TextBox();
            this.textBoxErrorConsole = new System.Windows.Forms.TextBox();
            this.textSignalIndicatorA = new System.Windows.Forms.TextBox();
            this.textSignalIndicatorB = new System.Windows.Forms.TextBox();
            this.textSignalIndicatorC = new System.Windows.Forms.TextBox();
            this.textSignalIndicatorD = new System.Windows.Forms.TextBox();
            this.textSignalIndicator0 = new System.Windows.Forms.TextBox();
            this.text_dBmN = new System.Windows.Forms.TextBox();
            this.textSignalIndicatorN0 = new System.Windows.Forms.TextBox();
            this.textSignalIndicatorND = new System.Windows.Forms.TextBox();
            this.textSignalIndicatorNC = new System.Windows.Forms.TextBox();
            this.textSignalIndicatorNB = new System.Windows.Forms.TextBox();
            this.textSignalIndicatorNA = new System.Windows.Forms.TextBox();
            this.textStateSignal = new System.Windows.Forms.TextBox();
            this.l_dBm = new System.Windows.Forms.Label();
            this.l_dBmN = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxConsole
            // 
            this.textBoxConsole.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxConsole.Location = new System.Drawing.Point(12, 12);
            this.textBoxConsole.Multiline = true;
            this.textBoxConsole.Name = "textBoxConsole";
            this.textBoxConsole.ReadOnly = true;
            this.textBoxConsole.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxConsole.Size = new System.Drawing.Size(1127, 235);
            this.textBoxConsole.TabIndex = 0;
            this.textBoxConsole.WordWrap = false;
            // 
            // btnStop
            // 
            this.btnStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(190, 382);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(160, 32);
            this.btnStop.TabIndex = 3;
            this.btnStop.Text = "Стоп";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStart
            // 
            this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStart.Location = new System.Drawing.Point(12, 381);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(172, 32);
            this.btnStart.TabIndex = 4;
            this.btnStart.Text = "Старт";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // checkBoxSimulation
            // 
            this.checkBoxSimulation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxSimulation.AutoSize = true;
            this.checkBoxSimulation.Location = new System.Drawing.Point(1017, 380);
            this.checkBoxSimulation.Name = "checkBoxSimulation";
            this.checkBoxSimulation.Size = new System.Drawing.Size(82, 17);
            this.checkBoxSimulation.TabIndex = 5;
            this.checkBoxSimulation.Text = "Симуляция";
            this.checkBoxSimulation.UseVisualStyleBackColor = true;
            // 
            // checkBoxACM
            // 
            this.checkBoxACM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxACM.AutoSize = true;
            this.checkBoxACM.Location = new System.Drawing.Point(1017, 358);
            this.checkBoxACM.Name = "checkBoxACM";
            this.checkBoxACM.Size = new System.Drawing.Size(122, 17);
            this.checkBoxACM.TabIndex = 6;
            this.checkBoxACM.Text = "Данные с антенны";
            this.checkBoxACM.UseVisualStyleBackColor = true;
            // 
            // textStateDate
            // 
            this.textStateDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textStateDate.Location = new System.Drawing.Point(12, 355);
            this.textStateDate.Name = "textStateDate";
            this.textStateDate.ReadOnly = true;
            this.textStateDate.Size = new System.Drawing.Size(89, 20);
            this.textStateDate.TabIndex = 7;
            // 
            // textStateTime
            // 
            this.textStateTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textStateTime.Location = new System.Drawing.Point(107, 355);
            this.textStateTime.Name = "textStateTime";
            this.textStateTime.ReadOnly = true;
            this.textStateTime.Size = new System.Drawing.Size(89, 20);
            this.textStateTime.TabIndex = 8;
            // 
            // textStateLat
            // 
            this.textStateLat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textStateLat.BackColor = System.Drawing.Color.White;
            this.textStateLat.Location = new System.Drawing.Point(202, 355);
            this.textStateLat.Name = "textStateLat";
            this.textStateLat.ReadOnly = true;
            this.textStateLat.Size = new System.Drawing.Size(89, 20);
            this.textStateLat.TabIndex = 9;
            // 
            // textStateLon
            // 
            this.textStateLon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textStateLon.BackColor = System.Drawing.Color.White;
            this.textStateLon.Location = new System.Drawing.Point(297, 355);
            this.textStateLon.Name = "textStateLon";
            this.textStateLon.ReadOnly = true;
            this.textStateLon.Size = new System.Drawing.Size(89, 20);
            this.textStateLon.TabIndex = 10;
            // 
            // textStatePressure
            // 
            this.textStatePressure.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textStatePressure.BackColor = System.Drawing.Color.White;
            this.textStatePressure.Location = new System.Drawing.Point(392, 355);
            this.textStatePressure.Name = "textStatePressure";
            this.textStatePressure.ReadOnly = true;
            this.textStatePressure.Size = new System.Drawing.Size(89, 20);
            this.textStatePressure.TabIndex = 11;
            // 
            // text_dBm
            // 
            this.text_dBm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.text_dBm.BackColor = System.Drawing.Color.White;
            this.text_dBm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.text_dBm.ForeColor = System.Drawing.Color.Black;
            this.text_dBm.Location = new System.Drawing.Point(785, 355);
            this.text_dBm.Name = "text_dBm";
            this.text_dBm.ReadOnly = true;
            this.text_dBm.Size = new System.Drawing.Size(48, 20);
            this.text_dBm.TabIndex = 12;
            // 
            // textBoxErrorConsole
            // 
            this.textBoxErrorConsole.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxErrorConsole.Location = new System.Drawing.Point(12, 253);
            this.textBoxErrorConsole.Multiline = true;
            this.textBoxErrorConsole.Name = "textBoxErrorConsole";
            this.textBoxErrorConsole.ReadOnly = true;
            this.textBoxErrorConsole.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxErrorConsole.Size = new System.Drawing.Size(1127, 89);
            this.textBoxErrorConsole.TabIndex = 16;
            // 
            // textSignalIndicatorA
            // 
            this.textSignalIndicatorA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textSignalIndicatorA.BackColor = System.Drawing.Color.Red;
            this.textSignalIndicatorA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textSignalIndicatorA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textSignalIndicatorA.ForeColor = System.Drawing.Color.Black;
            this.textSignalIndicatorA.Location = new System.Drawing.Point(699, 358);
            this.textSignalIndicatorA.Name = "textSignalIndicatorA";
            this.textSignalIndicatorA.ReadOnly = true;
            this.textSignalIndicatorA.Size = new System.Drawing.Size(20, 13);
            this.textSignalIndicatorA.TabIndex = 17;
            this.textSignalIndicatorA.Visible = false;
            // 
            // textSignalIndicatorB
            // 
            this.textSignalIndicatorB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textSignalIndicatorB.BackColor = System.Drawing.Color.DarkOrange;
            this.textSignalIndicatorB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textSignalIndicatorB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textSignalIndicatorB.ForeColor = System.Drawing.Color.Black;
            this.textSignalIndicatorB.Location = new System.Drawing.Point(719, 358);
            this.textSignalIndicatorB.Name = "textSignalIndicatorB";
            this.textSignalIndicatorB.ReadOnly = true;
            this.textSignalIndicatorB.Size = new System.Drawing.Size(20, 13);
            this.textSignalIndicatorB.TabIndex = 18;
            this.textSignalIndicatorB.Visible = false;
            // 
            // textSignalIndicatorC
            // 
            this.textSignalIndicatorC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textSignalIndicatorC.BackColor = System.Drawing.Color.Green;
            this.textSignalIndicatorC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textSignalIndicatorC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textSignalIndicatorC.ForeColor = System.Drawing.Color.Black;
            this.textSignalIndicatorC.Location = new System.Drawing.Point(739, 358);
            this.textSignalIndicatorC.Name = "textSignalIndicatorC";
            this.textSignalIndicatorC.ReadOnly = true;
            this.textSignalIndicatorC.Size = new System.Drawing.Size(20, 13);
            this.textSignalIndicatorC.TabIndex = 19;
            this.textSignalIndicatorC.Visible = false;
            // 
            // textSignalIndicatorD
            // 
            this.textSignalIndicatorD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textSignalIndicatorD.BackColor = System.Drawing.Color.Lime;
            this.textSignalIndicatorD.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textSignalIndicatorD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textSignalIndicatorD.ForeColor = System.Drawing.Color.Black;
            this.textSignalIndicatorD.Location = new System.Drawing.Point(759, 358);
            this.textSignalIndicatorD.Name = "textSignalIndicatorD";
            this.textSignalIndicatorD.ReadOnly = true;
            this.textSignalIndicatorD.Size = new System.Drawing.Size(20, 13);
            this.textSignalIndicatorD.TabIndex = 20;
            this.textSignalIndicatorD.Visible = false;
            // 
            // textSignalIndicator0
            // 
            this.textSignalIndicator0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textSignalIndicator0.BackColor = System.Drawing.Color.DarkRed;
            this.textSignalIndicator0.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textSignalIndicator0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textSignalIndicator0.ForeColor = System.Drawing.Color.Black;
            this.textSignalIndicator0.Location = new System.Drawing.Point(690, 358);
            this.textSignalIndicator0.Name = "textSignalIndicator0";
            this.textSignalIndicator0.ReadOnly = true;
            this.textSignalIndicator0.Size = new System.Drawing.Size(9, 13);
            this.textSignalIndicator0.TabIndex = 21;
            this.textSignalIndicator0.Visible = false;
            // 
            // text_dBmN
            // 
            this.text_dBmN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.text_dBmN.BackColor = System.Drawing.Color.White;
            this.text_dBmN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.text_dBmN.ForeColor = System.Drawing.Color.Black;
            this.text_dBmN.Location = new System.Drawing.Point(785, 379);
            this.text_dBmN.Name = "text_dBmN";
            this.text_dBmN.ReadOnly = true;
            this.text_dBmN.Size = new System.Drawing.Size(48, 20);
            this.text_dBmN.TabIndex = 22;
            // 
            // textSignalIndicatorN0
            // 
            this.textSignalIndicatorN0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textSignalIndicatorN0.BackColor = System.Drawing.Color.DarkRed;
            this.textSignalIndicatorN0.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textSignalIndicatorN0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textSignalIndicatorN0.ForeColor = System.Drawing.Color.Black;
            this.textSignalIndicatorN0.Location = new System.Drawing.Point(690, 382);
            this.textSignalIndicatorN0.Name = "textSignalIndicatorN0";
            this.textSignalIndicatorN0.ReadOnly = true;
            this.textSignalIndicatorN0.Size = new System.Drawing.Size(9, 13);
            this.textSignalIndicatorN0.TabIndex = 27;
            this.textSignalIndicatorN0.Visible = false;
            // 
            // textSignalIndicatorND
            // 
            this.textSignalIndicatorND.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textSignalIndicatorND.BackColor = System.Drawing.Color.Lime;
            this.textSignalIndicatorND.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textSignalIndicatorND.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textSignalIndicatorND.ForeColor = System.Drawing.Color.Black;
            this.textSignalIndicatorND.Location = new System.Drawing.Point(759, 382);
            this.textSignalIndicatorND.Name = "textSignalIndicatorND";
            this.textSignalIndicatorND.ReadOnly = true;
            this.textSignalIndicatorND.Size = new System.Drawing.Size(20, 13);
            this.textSignalIndicatorND.TabIndex = 26;
            this.textSignalIndicatorND.Visible = false;
            // 
            // textSignalIndicatorNC
            // 
            this.textSignalIndicatorNC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textSignalIndicatorNC.BackColor = System.Drawing.Color.Green;
            this.textSignalIndicatorNC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textSignalIndicatorNC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textSignalIndicatorNC.ForeColor = System.Drawing.Color.Black;
            this.textSignalIndicatorNC.Location = new System.Drawing.Point(739, 382);
            this.textSignalIndicatorNC.Name = "textSignalIndicatorNC";
            this.textSignalIndicatorNC.ReadOnly = true;
            this.textSignalIndicatorNC.Size = new System.Drawing.Size(20, 13);
            this.textSignalIndicatorNC.TabIndex = 25;
            this.textSignalIndicatorNC.Visible = false;
            // 
            // textSignalIndicatorNB
            // 
            this.textSignalIndicatorNB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textSignalIndicatorNB.BackColor = System.Drawing.Color.DarkOrange;
            this.textSignalIndicatorNB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textSignalIndicatorNB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textSignalIndicatorNB.ForeColor = System.Drawing.Color.Black;
            this.textSignalIndicatorNB.Location = new System.Drawing.Point(719, 382);
            this.textSignalIndicatorNB.Name = "textSignalIndicatorNB";
            this.textSignalIndicatorNB.ReadOnly = true;
            this.textSignalIndicatorNB.Size = new System.Drawing.Size(20, 13);
            this.textSignalIndicatorNB.TabIndex = 24;
            this.textSignalIndicatorNB.Visible = false;
            // 
            // textSignalIndicatorNA
            // 
            this.textSignalIndicatorNA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textSignalIndicatorNA.BackColor = System.Drawing.Color.Red;
            this.textSignalIndicatorNA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textSignalIndicatorNA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textSignalIndicatorNA.ForeColor = System.Drawing.Color.Black;
            this.textSignalIndicatorNA.Location = new System.Drawing.Point(699, 382);
            this.textSignalIndicatorNA.Name = "textSignalIndicatorNA";
            this.textSignalIndicatorNA.ReadOnly = true;
            this.textSignalIndicatorNA.Size = new System.Drawing.Size(20, 13);
            this.textSignalIndicatorNA.TabIndex = 23;
            this.textSignalIndicatorNA.Visible = false;
            // 
            // textStateSignal
            // 
            this.textStateSignal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textStateSignal.BackColor = System.Drawing.Color.White;
            this.textStateSignal.Location = new System.Drawing.Point(487, 355);
            this.textStateSignal.Name = "textStateSignal";
            this.textStateSignal.ReadOnly = true;
            this.textStateSignal.Size = new System.Drawing.Size(89, 20);
            this.textStateSignal.TabIndex = 28;
            // 
            // l_dBm
            // 
            this.l_dBm.AutoSize = true;
            this.l_dBm.Location = new System.Drawing.Point(582, 358);
            this.l_dBm.Name = "l_dBm";
            this.l_dBm.Size = new System.Drawing.Size(102, 13);
            this.l_dBm.TabIndex = 29;
            this.l_dBm.Text = "Дальнаяя антенна";
            // 
            // l_dBmN
            // 
            this.l_dBmN.AutoSize = true;
            this.l_dBmN.Location = new System.Drawing.Point(588, 382);
            this.l_dBmN.Name = "l_dBmN";
            this.l_dBmN.Size = new System.Drawing.Size(96, 13);
            this.l_dBmN.TabIndex = 30;
            this.l_dBmN.Text = "Ближняя антенна";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1151, 421);
            this.Controls.Add(this.l_dBmN);
            this.Controls.Add(this.l_dBm);
            this.Controls.Add(this.textStateSignal);
            this.Controls.Add(this.textSignalIndicatorN0);
            this.Controls.Add(this.textSignalIndicatorND);
            this.Controls.Add(this.textSignalIndicatorNC);
            this.Controls.Add(this.textSignalIndicatorNB);
            this.Controls.Add(this.textSignalIndicatorNA);
            this.Controls.Add(this.text_dBmN);
            this.Controls.Add(this.textSignalIndicator0);
            this.Controls.Add(this.textSignalIndicatorD);
            this.Controls.Add(this.textSignalIndicatorC);
            this.Controls.Add(this.textSignalIndicatorB);
            this.Controls.Add(this.textSignalIndicatorA);
            this.Controls.Add(this.textBoxErrorConsole);
            this.Controls.Add(this.text_dBm);
            this.Controls.Add(this.textStatePressure);
            this.Controls.Add(this.textStateLon);
            this.Controls.Add(this.textStateLat);
            this.Controls.Add(this.textStateTime);
            this.Controls.Add(this.textStateDate);
            this.Controls.Add(this.checkBoxACM);
            this.Controls.Add(this.checkBoxSimulation);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.textBoxConsole);
            this.Name = "MainForm";
            this.Text = "Мониторинг параметров полета";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxConsole;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.CheckBox checkBoxSimulation;
        private System.Windows.Forms.CheckBox checkBoxACM;
        private System.Windows.Forms.TextBox textStateDate;
        private System.Windows.Forms.TextBox textStateTime;
        private System.Windows.Forms.TextBox textStateLat;
        private System.Windows.Forms.TextBox textStateLon;
        private System.Windows.Forms.TextBox textStatePressure;
        private System.Windows.Forms.TextBox text_dBm;
        private System.Windows.Forms.TextBox textBoxErrorConsole;
        private System.Windows.Forms.TextBox textSignalIndicatorA;
        private System.Windows.Forms.TextBox textSignalIndicatorB;
        private System.Windows.Forms.TextBox textSignalIndicatorC;
        private System.Windows.Forms.TextBox textSignalIndicatorD;
        private System.Windows.Forms.TextBox textSignalIndicator0;
        private System.Windows.Forms.TextBox text_dBmN;
        private System.Windows.Forms.TextBox textSignalIndicatorN0;
        private System.Windows.Forms.TextBox textSignalIndicatorND;
        private System.Windows.Forms.TextBox textSignalIndicatorNC;
        private System.Windows.Forms.TextBox textSignalIndicatorNB;
        private System.Windows.Forms.TextBox textSignalIndicatorNA;
        private System.Windows.Forms.TextBox textStateSignal;
        private System.Windows.Forms.Label l_dBm;
        private System.Windows.Forms.Label l_dBmN;
    }
}

